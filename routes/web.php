<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('register', 'HomeController@redirectToLogin')->name('register');
Route::post('register', 'HomeController@redirectToLogin')->name('register');

Route::middleware(['auth'])->group(function () {

    Route::get('/', 'HomeController@index')->name('home');

    Route::get('cars/expenses', 'CarExpenseController@all')->name('cars.expenses');
    Route::get('cars/{car}/expenses/{expense}/verified', 'CarExpenseController@verified')->name('cars.expenses.verified');
    Route::get('receivedOrders/{receivedOrder}/issuedOrders/{issuedOrder}/freights/{freight}/replicate', 'ReceivedOrderIssuedOrderFreightController@replicate')->name('receivedOrders.issuedOrders.freights.replicate');
    Route::get('receivedOrders/{receivedOrder}/shipments/{shipment}/freights/{freight}/replicate', 'ReceivedOrderShipmentFreightController@replicate')->name('receivedOrders.shipments.freights.replicate');
    Route::get('issuedOrders/{issuedOrder}/pdfOpen', 'IssuedOrderController@pdfOpen')->name('issuedOrders.pdfOpen');
    Route::get('issuedOrders/{issuedOrder}/pdfDownload', 'IssuedOrderController@pdfDownload')->name('issuedOrders.pdfDownload');
    Route::get('invoices/{invoice}/pdfOpen', 'InvoiceController@pdfOpen')->name('invoices.pdfOpen');
    Route::get('invoices/{invoice}/pdfDownload', 'InvoiceController@pdfDownload')->name('invoices.pdfDownload');
    Route::get('settings/profile', 'UserController@profile')->name('settings.profile');

    Route::resources([
        'cars' => 'CarController',
        'cars.expenses' => 'CarExpenseController',
        'expenses' => 'ExpenseController',
        'clients' => 'ClientController',
        'carriers' => 'CarrierController',
        'drivers' => 'DriverController',
        'drivers.deposits' => 'DriverDepositController',
        'invoices' => 'InvoiceController',
        'invoices.details' => 'InvoiceDetailController',
        'trips' => 'TripController',
        'trips.expenses' => 'TripExpenseController',
        'trips.shipments' => 'TripShipmentController',
        'trips.deposits' => 'TripDepositController',
        'receivedOrders' => 'ReceivedOrderController',
        'issuedOrders' => 'IssuedOrderController',
        'receivedOrders.freights' => 'ReceivedOrderFreightController',
        'receivedOrders.shipments' => 'ReceivedOrderShipmentController',
        'receivedOrders.shipments.freights' => 'ReceivedOrderShipmentFreightController',
        'receivedOrders.issuedOrders' => 'ReceivedOrderIssuedOrderController',
        'receivedOrders.issuedOrders.freights' => 'ReceivedOrderIssuedOrderFreightController',

    ]);


    Route::prefix('data')->name('data.')->group(function () {
        Route::get('cars', 'CarController@data')->name('cars');
        Route::get('clients', 'ClientController@data')->name('clients');
        Route::get('clients/{client}', 'ClientController@single')->name('client');
        Route::get('carriers', 'CarrierController@data')->name('carriers');
        Route::get('drivers', 'DriverController@data')->name('drivers');
        Route::get('invoices', 'InvoiceController@data')->name('invoices');
        Route::get('invoices/{invoice}/receivedOrders/', 'InvoiceDetailController@dataReceivedOrdersNoInvoice')->name('receivedOrdersNoInvoice');
        Route::post('invoices/{invoice}/receivedOrders/', 'InvoiceDetailController@attachReceivedOrderToInvoice')->name('attachDetails');
        Route::get('invoice/max', 'HomeController@null')->name('invoice.max');
        Route::get('invoice/max/{series}', 'invoiceController@maxNumber')->name('invoice.max.number');
        Route::get('exchange', 'HomeController@null')->name('exchange');
        Route::get('exchange/{day}/{month}/{year}', 'invoiceController@exchangeRate')->name('exchangeRate');
        Route::get('drivers/{driver}/deposits', 'DriverDepositController@data')->name('drivers.deposits');

        //trips

        Route::prefix('trips')->name('trips.')->group(function () {

            Route::get('', 'TripController@data')->name('index');

            Route::prefix('{trip}')->group(function () {

                //shipments

                Route::get('shipments', 'TripShipmentController@shipmentsData')->name('shipments');
                Route::get('carShipments', 'TripShipmentController@carShipmentsData')->name('carShipments');
                Route::post('detachShipments', 'TripShipmentController@detachShipments')->name('detachShipments');
                Route::post('attachShipments', 'TripShipmentController@attachShipments')->name('attachShipments');

                //deposits

                Route::get('deposits', 'TripDepositController@depositsData')->name('deposits');
                Route::get('driverDeposits', 'TripDepositController@driverDepositsData')->name('driverDeposits');
                Route::post('detachDeposits', 'TripDepositController@detachDeposits')->name('detachDeposits');
                Route::post('attachDeposits', 'TripDepositController@attachDeposits')->name('attachDeposits');

                //expenses
                Route::get('expenses', 'TripExpenseController@data')->name('expenses');
            });
        });

        Route::get('cars/expenses', 'CarExpenseController@dataAll')->name('cars.all.expenses');
        Route::get('cars/{car}/expenses', 'CarExpenseController@data')->name('cars.expenses');

        Route::get('drivers/{driver}/tripDeposits', 'DriverController@shipmentsData')->name('driver.deposits');
        Route::post('drivers/{driver}/tripDeposits/{trip}', 'DriverController@shipmentsTrip')->name('driver.deposits.trip');
        Route::get('expenses', 'ExpenseController@data')->name('expenses');
        Route::get('receivedOrders', 'ReceivedOrderController@data')->name('receivedOrders');
        Route::get('issuedOrders', 'IssuedOrderController@data')->name('issuedOrders');
        Route::get('settings/countries', 'CountryController@data')->name('settings.countries');
        Route::get('settings/users', 'UserController@data')->name('settings.users');
    });

    Route::prefix('log')->name('log.')->group(function () {

        Route::get('receivedOrders/{receivedOrder}', 'ReceivedOrderController@log')->name('receivedOrders');

    });

//    Route::resource('car.expenses', 'CarExpenseController');
//    Route::resource('v', 'ReceivedOrderController');
    Route::resource('settings/countries', 'CountryController', ['as' => 'settings']);
    Route::resource('settings/users', 'UserController', ['as' => 'settings']);
//    Route::resource('receivedOrders.freights', 'ReceivedOrderFreightController');

    Route::get('mail', 'DevController@mail')->name('mail');
//    Route::get('issuedOrders', 'DevController@issuedOrders')->name('issuedOrders');
    Route::get('pdf', 'DevController@pdf')->name('pdf');
    Route::get('db2', 'DevController@db2')->name('db2');
//    Route::get('api', 'DevController@api')->name('api');
    Route::get('test', 'DevController@test')->name('test');
    Route::get('test2', 'DevController@test2')->name('test2');
    Route::get('rate', 'DevController@rate')->name('rate');
    Route::get('form', 'DevController@form')->name('form');
    Route::post('form', 'DevController@post')->name('form');

});


