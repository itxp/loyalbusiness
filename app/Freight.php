<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class Freight extends Model
{
    //
    use LogsActivity;

    protected static $logAttributes = ['*'];

    protected $fillable = [
        'freight',
        'reference',
        'description',
        'load_date',
        'load_hours',
        'load_country_id',
        'load_address',
        'load_company',
        'unload_date',
        'unload_hours',
        'unload_country_id',
        'unload_address',
        'unload_company'
    ];

    public function setLoadDateAttribute($value)
    {
        $date = \DateTime::createFromFormat('d/m/Y', $value);

        $this->attributes['load_date'] =  $date->format('Y-m-d');
    }

    public function getLoadDateAttribute($value)
    {
        $date = \DateTime::createFromFormat('Y-m-d', $value);

        return $date->format('d/m/Y');
    }

    public function setUnloadDateAttribute($value)
    {
        $date = \DateTime::createFromFormat('d/m/Y', $value);

        $this->attributes['unload_date'] =  $date->format('Y-m-d');
    }

    public function getUnloadDateAttribute($value)
    {
        $date = \DateTime::createFromFormat('Y-m-d', $value);

        return $date->format('d/m/Y');
    }

    public function commentable()
    {
        return $this->morphTo();
    }

    public function loadCountry()
    {
        return $this->belongsTo('App\Country', 'load_country_id', 'id');
    }

    public function unloadCountry()
    {
        return $this->belongsTo('App\Country', 'unload_country_id', 'id');
    }
}
