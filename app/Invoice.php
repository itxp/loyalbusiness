<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class Invoice extends Model
{
    //
    use LogsActivity;

    protected static $logAttributes = ['*'];

    protected static $logOnlyDirty = true;

    protected $casts = [
        'delegate' => 'array',
        'buyer' => 'array',
        'details' => 'array',
    ];

    protected $fillable = [
        'series',
        'number',
        'date',
        'client_id',
        'details',
        'total_ron',
        'total_eur',
        'exchange_rate',
        'vat',
        'discount',
        'due_date',
        'delegate',
        'user_id'
    ];


    public function setDateAttribute($value)
    {
        $date = \DateTime::createFromFormat('d/m/Y', $value);

        $this->attributes['date'] =  $date->format('Y-m-d');
    }

    public function getDateAttribute($value)
    {
        $date = \DateTime::createFromFormat('Y-m-d', $value);

        return $date->format('d/m/Y');
    }

    public function setDueDateAttribute($value)
    {
        $date = \DateTime::createFromFormat('d/m/Y', $value);

        $this->attributes['due_date'] =  $date->format('Y-m-d');
    }

    public function getDueDateAttribute($value)
    {
        $date = \DateTime::createFromFormat('Y-m-d', $value);

        return $date->format('d/m/Y');
    }

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
