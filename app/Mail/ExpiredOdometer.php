<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ExpiredOdometer extends Mailable
{
    use Queueable, SerializesModels;

    public $expenses;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($expenses)
    {
        //
        $this->expenses = $expenses;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@loialbusiness.ro', 'Notificari AdminLBM')
            ->subject('Notificare: Au depasit limita de km ...')
            ->markdown('emails.expired-odometer');
    }
}
