<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class Expense extends Model
{
    //
    use LogsActivity;

    protected static $logAttributes = ['*'];

    protected $fillable = [
        'name',
        'description',
        'cost',
        'currency',
        'date',
        'expiry_date',
        'expiry_odometer',
        'odometer',
        'user_id',
        'car_id',
        'trip_id',
        'source',
    ];

    public function setDateAttribute($value)
    {
        $date = \DateTime::createFromFormat('d/m/Y', $value);

        $this->attributes['date'] =  $date->format('Y-m-d');
    }

    public function getDateAttribute($value)
    {
        $date = \DateTime::createFromFormat('Y-m-d', $value);

        return $date->format('d/m/Y');
    }

    public function setExpiryDateAttribute($value)
    {
        if($value){
            $date = \DateTime::createFromFormat('d/m/Y', $value);

            $this->attributes['expiry_date'] =  $date->format('Y-m-d');
        }
        else return $value;
    }

    public function getExpiryDateAttribute($value)
    {
        if($value){
            $date = \DateTime::createFromFormat('Y-m-d', $value);
            return $date->format('d/m/Y');
        }
        else return $value;

    }
//    protected function getDateFormat()
//    {
//        return 'Y/m/d';
//    }

    public function car()
    {
        return $this->belongsTo('App\Car');
    }

    public function trip()
    {
        return $this->belongsTo('App\Trip');
    }

    public function scopeValid($query)
    {
        return $query->where('expired', false);
    }

    public function scopeExpired($query)
    {
        return $query->where('expired', true);
    }

    public function scopeExpiryOdometer($query)
    {
        return $query->whereNotNull('expiry_odometer');
    }

    public function scopeExpiryDate($query)
    {
        return $query->whereNotNull('expiry_date');
    }

    public function scopeIsCar($query)
    {
        return $query->whereNotNull('car_id');
    }

    public function scopeIsTrip($query)
    {
        return $query->whereNotNull('trip_id');
    }

    public function scopeIsCompany($query)
    {
        return $query->whereNull('car_id')->whereNull('trip_id');
    }

    public function scopeDkv($query)
    {
        return $query->where('source','DKV');
    }

    public function scopeCash($query)
    {
        return $query->where('source','CASH');
    }

    public function scopePersonal($query)
    {
        return $query->where('source','PERSONAL CARD');
    }

    public function scopeCompany($query)
    {
        return $query->where('source','COMPANY CARD');
    }

    public function scopeEur($query)
    {
        return $query->where('currency','EUR');
    }

    public function scopeRon($query)
    {
        return $query->where('currency','RON');
    }

//    public function scopePendingExpire($query)
//    {
//        re
//    }

}
