<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class IssuedOrder extends Model
{
    //
    use LogsActivity;

    protected static $logAttributes = ['*'];

    protected $fillable = [
        'number',
        'carrier_id',
        'date',
        'price',
        'currency',
        'description',
        'type',
        'plate',
        'driver',
        'tos',
        'user_id'
    ];

    public function setDateAttribute($value)
    {
        $date = \DateTime::createFromFormat('d/m/Y', $value);

        $this->attributes['date'] =  $date->format('Y-m-d H:i:s');
    }

    public function getDateAttribute($value)
    {
        $date = \DateTime::createFromFormat('Y-m-d', $value);

        return $date->format('d/m/Y');
    }

    public function carrier()
    {
        return $this->belongsTo('App\Carrier');
    }

    public function freights()
    {
        return $this->morphMany('App\Freight', 'freightable');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($issuedOrder) {
            $issuedOrder->freights()->delete();
        });
    }
}
