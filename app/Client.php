<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class Client extends Model
{
    //
    use LogsActivity;

    protected static $logAttributes = ['*'];

    protected $fillable = [
        'name',
        'cif',
        'orc',
        'country_id',
        'address',
        'bank',
        'iban',
        'swift',
        'phone',
        'email',
        'contact',
        'discount'
    ];

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function receivedOrders()
    {
        return $this->hasMany('App\ReceivedOrder');
    }
}
