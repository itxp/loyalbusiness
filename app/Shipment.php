<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class Shipment extends Model
{
    //
    use LogsActivity;

    protected static $logAttributes = ['*'];


    protected $fillable = [
        'car_id',
        'trip_id',
        'number',
        'price',
        'distance_unloaded',
        'distance_loaded',
    ];

    public function car()
    {
        return $this->belongsTo('App\Car');
    }

    public function freights()
    {
        return $this->morphMany('App\Freight', 'freightable');
    }

    public function scopeEur($query)
    {
        return $query->where('currency','EUR');
    }

    public function scopeRon($query)
    {
        return $query->where('currency','RON');
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($shipment) {
            $shipment->freights()->delete();
        });
    }
}
