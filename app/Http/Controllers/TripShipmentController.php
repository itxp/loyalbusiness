<?php

namespace App\Http\Controllers;

use App\Shipment;
use App\Trip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class TripShipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Trip $trip)
    {
        //
        return view('trips.shipments.index', compact('trip'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function shipmentsData(Trip $trip){

        $shipments = $trip
            ->shipments()
            ->with('freights.loadCountry', 'freights.unloadCountry');

        return Datatables::of($shipments)

            ->addColumn('route', function ($shipment)  {
                $return = '';

                foreach ($shipment->freights as $freight) {
                    $return = $return.$freight->loadCountry->iso.' - '.$freight->unloadCountry->iso.' | ';
                }

                return $return;
            })
            ->addColumn('action', function ($shipment) {
                return '<a href="'.route('receivedOrders.shipments.show', [ $shipment->received_order_id, $shipment->id ]).'" class="btn btn-xs btn"><i class="glyphicon glyphicon-file"></i> Deschide</a>';
            })
            ->make(true);

    }

    public function carShipmentsData(Trip $trip){

        $shipments = $trip
            ->car
            ->shipments()
            ->whereNull('trip_id')
            ->with('freights.loadCountry', 'freights.unloadCountry');

        return Datatables::of($shipments)

            ->addColumn('route', function ($shipment)  {

                $return = '';

                foreach ($shipment->freights as $freight) {
                    $return = $return.'['.$freight->loadCountry->iso.'] '.$freight->load_date.' - '.'['.$freight->unloadCountry->iso.'] '.$freight->unload_date.' | ';
                }

                return $return;
            })
            ->addColumn('action', function ($shipment) {
                return '<a href="'.route('receivedOrders.shipments.show', [ $shipment->received_order_id, $shipment->id ]).'" class="btn btn-xs btn"><i class="glyphicon glyphicon-file"></i> Deschide</a>';
            })
            ->make(true);

    }

    public function attachShipments($trip_id, Request $request){

        $shipments = $request->input('shipments');
        if($shipments)
            Shipment::
                whereIn('id', $shipments)
                ->update(['trip_id' => $trip_id]);
        return 'ok';
    }

    public function detachShipments($trip_id, Request $request){

        $shipments = $request->input('shipments');
        if($shipments)
        DB::table('shipments')
            ->whereIn('id', $shipments)
            ->update(['trip_id' => null]);
        return 'ok';
    }


}
