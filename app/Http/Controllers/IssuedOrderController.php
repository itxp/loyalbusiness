<?php

namespace App\Http\Controllers;

use App\IssuedOrder;
use App\Option;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class IssuedOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('issuedOrders.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IssuedOrder  $issuedOrder
     * @return \Illuminate\Http\Response
     */
    public function show(IssuedOrder $issuedOrder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IssuedOrder  $issuedOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(IssuedOrder $issuedOrder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IssuedOrder  $issuedOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IssuedOrder $issuedOrder)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IssuedOrder  $issuedOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(IssuedOrder $issuedOrder)
    {
        //
    }

    public function data()
    {

        $issuedOrders = IssuedOrder::join('carriers', 'issued_orders.carrier_id', 'carriers.id')
            ->select('issued_orders.id','issued_orders.number','issued_orders.received_order_id','carriers.name', 'issued_orders.date', 'issued_orders.price', 'issued_orders.currency');

        return DataTables::of($issuedOrders)
            ->addColumn('action', function ($issuedOrder) {
//                dd($receivedOrder);
                return '<a href="'.route('receivedOrders.issuedOrders.show', [$issuedOrder->received_order_id, $issuedOrder->id]).'" class="btn btn-xs btn"><i class="glyphicon glyphicon-file"></i> Deschide</a>';
            })
            ->filterColumn('date', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(issued_orders.date,'%d/%m/%Y') like ?", ["%$keyword%"]);
            })
            ->make(true);

    }

    public function pdfOpen(IssuedOrder $issuedOrder)
    {
        $header = Option::where('key','header')->first()->value;
        $pdf = PDF::loadView('issuedOrders.pdf', compact('issuedOrder','header'))->setPaper('a4', 'portrait');
        return $pdf->stream();
    }

    public function pdfDownload(IssuedOrder $issuedOrder)
    {
        $header = Option::where('key','header')->first()->value;
        $pdf = PDF::loadView('issuedOrders.pdf', compact('issuedOrder','header'))->setPaper('a4', 'portrait');
        return $pdf->download('comanda#'.$issuedOrder->id.'.pdf');
    }
}
