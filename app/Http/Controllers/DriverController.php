<?php

namespace App\Http\Controllers;

use App\Driver;
use App\Http\Requests\AddDriverRequest;
use Yajra\DataTables\DataTables;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('drivers.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('drivers.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddDriverRequest $request)
    {
        //
        Driver::create($request->all());

        return redirect()->route('drivers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function show(Driver $driver)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function edit(Driver $driver)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Driver $driver)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function destroy(Driver $driver)
    {
        //
    }

    public function data()
    {
        //
        $drivers = Driver::select('id', 'name', 'description');

        return Datatables::of($drivers)
            ->addColumn('action', function ($driver) {
                return '<a href="'.route('drivers.deposits.create', [$driver->id]).'" class="btn btn-xs btn"><i class="glyphicon glyphicon-plus"></i>Adauga depunere</a>
                        <a href="'.route('drivers.deposits.index', [$driver->id]).'" class="btn btn-xs btn-primary">Depuneri</a>';
            })
            ->make(true);
    }
}
