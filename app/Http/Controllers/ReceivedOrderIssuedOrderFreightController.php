<?php

namespace App\Http\Controllers;

use App\Freight;
use App\IssuedOrder;
use App\ReceivedOrder;
use Illuminate\Http\Request;

class ReceivedOrderIssuedOrderFreightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReceivedOrder $receivedOrder, IssuedOrder $issuedOrder, Freight $freight)
    {
        //
        $freight->delete();

        return redirect(route('receivedOrders.issuedOrders.show', [$receivedOrder, $issuedOrder]));
    }

    public function replicate(ReceivedOrder $receivedOrder, IssuedOrder $issuedOrder, Freight $freight)
    {
        $issuedOrder->freights()->save($freight->replicate(['freightable_id','freightable_type']));

        return redirect(route('receivedOrders.issuedOrders.show', [$receivedOrder, $issuedOrder]));

    }
}
