<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddInvoiceDetailRequest;
use App\Invoice;
use App\ReceivedOrder;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class InvoiceDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Invoice $invoice)
    {
        //

        return view('invoices.details.create', compact('invoice'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Invoice $invoice, AddInvoiceDetailRequest $request)
    {
        //
//        dd(is_null()$invoice->details);
////        dd($request->all('description','unit','quantity','price_eur','price_ron'));
//        $details = $invoice->details;
//        array_push($old[], $request->all('description','unit','quantity','price_eur','price_ron'));
//        $invoice->details = $old;
////        dd($request->all());

        $this->details($invoice, [$request->all('description','unit','quantity','price_eur','price_ron','received_order_id')]);

        return redirect()->route('invoices.show', [$invoice->id]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice, $id)
    {
        //
        $invoiceDetails = $invoice->details;
//        dd($details[$id]['received_order_id']);
        if(!is_null($invoiceDetails[$id]['received_order_id']))
            ReceivedOrder::where('id', $invoiceDetails[$id]['received_order_id'])->update(['invoice_id' => null]);

        unset($invoiceDetails[$id]);
        $invoice->details = $invoiceDetails;

        $this->recalculateAndSave($invoice);

        return 'ok';
    }

    public function dataReceivedOrdersNoInvoice(Invoice $invoice){

        $receivedOrders = $invoice->client->receivedOrders()->noInvoice()
            ->select('id','number', 'date', 'price', 'currency');

        return Datatables::of($receivedOrders)
            ->addColumn('action', function ($receivedOrder) {
//                dd($receivedOrder);
                return '<a href="'.route('receivedOrders.show', [$receivedOrder->id]).'" class="btn btn-xs btn"><i class="glyphicon glyphicon-file"></i> Deschide</a>';
            })
            ->filterColumn('date', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(received_orders.date,'%d/%m/%Y') like ?", ["%$keyword%"]);
            })
            ->make(true);

    }

    public function attachReceivedOrderToInvoice(Invoice $invoice, Request $request){

        $ids = $request->input('ids');

//        $invoice_details = $invoice->details;
//
//        if(is_null($invoice_details)){
//            $details = [$request->all('description','unit','quantity','price_eur','price_ron','received_order_id')];
//        }
//        else{
//            array_push($invoice_details, $request->all('description','unit','quantity','price_eur','price_ron','received_order_id'));
//        };

        $details = [];
        if($ids){
            ReceivedOrder::whereIn('id', $ids)->update(['invoice_id' => $invoice->id]);
            $receivedOrders = ReceivedOrder::whereIn('id', $ids)->with('freights')->get();
            foreach ($receivedOrders as $receivedOrder){
                $description = 'Transport intracomunitar<br>Nr Comanda: '.$receivedOrder->number.' Extra/OPT: '.$receivedOrder->opt.'Data: '.$receivedOrder->date.'<br>';
                foreach ($receivedOrder->freights as $freight){
                    $description .= 'Ruta: '.$freight->load_company.'['.$freight->loadCountry->name.'] - '.$freight->unload_company.'['.$freight->unloadCountry->name.']<br>';
                }
                $unit = 'TR';
                $quantity = 1;
                switch ($receivedOrder->currency) {
                    case 'EUR': $price_eur = $receivedOrder->price; $price_ron = $price_eur * $invoice->exchange_rate;
                    case 'RON': $price_ron = $receivedOrder->price; $price_eur = $price_ron / $invoice->exchange_rate;
                }
                $received_order_id = $receivedOrder->id;
                array_push($details, compact('description', 'unit', 'quantity', 'price_eur', 'price_ron', 'received_order_id'));
            }
//            dd($details);
            $this->details($invoice, $details);
        }

        return 'ok';
    }

    private function details(Invoice $invoice, $details){
        $invoice_details = $invoice->details;

        if(empty($invoice_details)){
            $invoice_details = $details;
        }
        else{
            $invoice_details = array_merge($invoice_details, $details);
        };

        $invoice->details = $invoice_details;

        $this->recalculateAndSave($invoice);
    }

    private function recalculateAndSave($invoice){

        $total_ron = 0;
        $total_eur = 0;

        foreach ($invoice->details as $detail){
            $total_eur += $detail['price_eur'];
            $total_ron += $detail['price_ron'];
        }

        $calc = ($invoice->vat+100)/100*(100-$invoice->discount)/100;
        $invoice->total_eur = $total_eur*$calc;
        $invoice->total_ron = $total_ron*$calc;

        $invoice->update();
    }
}
