<?php

namespace App\Http\Controllers;

use App\Country;
use App\Freight;
use App\Http\Requests\AddReceivedOrderFreightRequest;
use App\ReceivedOrder;
use Illuminate\Http\Request;

class ReceivedOrderFreightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ReceivedOrder $receivedOrder)
    {
        //
        $countries = Country::orderBy('name')->get();
        return view('receivedOrders.freights.create', compact('receivedOrder', 'countries'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReceivedOrder $receivedOrder, AddReceivedOrderFreightRequest $request)
    {
        //
//        dd($request->all());
        $receivedOrder->freights()->create($request->all());
//return true;
        return redirect()->route('receivedOrders.show', [$receivedOrder->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReceivedOrder $receivedOrder, Freight $freight)
    {
        //

        $freight->delete();

        return redirect(route('receivedOrders.show', [ $receivedOrder ]));
    }
}
