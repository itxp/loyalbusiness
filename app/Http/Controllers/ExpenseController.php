<?php

namespace App\Http\Controllers;

use App\Expense;
use App\Http\Requests\AddExpenseRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('expenses.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('expenses.create', compact('car'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddExpenseRequest $request)
    {
        //
        Expense::create($request->all());
        return redirect(route('expenses.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function data()
    {
        //
//        dd($car->expenses());

        $expenses = Expense::isCompany()->select('expenses.name', 'expenses.date', 'expenses.cost', 'expenses.currency');

//        dd($expenses->get());
//        { data: 'plate', name: 'plate' },
//        { data: 'name', name: 'name' },
//        { data: 'date', name: 'date' },
//        { data: 'cost', name: 'cost' },

        return Datatables::of($expenses)
//            ->addColumn('action', function ($car) {
//                return '<a href="'.route('cars.expenses.index', [$car->id]).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Cheltuieli</a>
//                        <a href="'.route('cars.edit', [$car->id]).'" class="btn btn-xs btn"><i class="glyphicon glyphicon-edit"></i> Modifica</a>';
//            })
//            ->editColumn('expenses.date', function ($expense) {
//                return $expense->date ? with(new Carbon($expense->date))->format('d/m/Y') : '';
//            })
//            ->filterColumn('expenses.date', function ($query, $keyword) {
//                $query->whereRaw("DATE_FORMAT(date,'%d/%m/%Y') like ?", ["%$keyword%"]);
//            })
//            ->editColumn('expenses.date', function ($expense) {
//                return $expense->date->format('d/m/Y');
//            })
            ->filterColumn('date', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(expenses.date,'%d/%m/%Y') like ?", ["%$keyword%"]);
            })
            ->make(true);
    }
}
