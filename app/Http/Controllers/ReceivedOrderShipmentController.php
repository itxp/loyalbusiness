<?php

namespace App\Http\Controllers;

use App\Car;
use App\Http\Requests\AddReceivedOrderShipmentRequest;
use App\ReceivedOrder;
use App\Shipment;

class ReceivedOrderShipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ReceivedOrder $receivedOrder)
    {
        //
        $cars = Car::orderBy('plate')->get();
        return view('receivedOrders.shipments.create', compact('receivedOrder', 'cars'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReceivedOrder $receivedOrder, AddReceivedOrderShipmentRequest $request)
    {
        //
//        dd($request);
        $shipment = $receivedOrder->shipments()->create($request->all());
        return redirect(route('receivedOrders.shipments.show', [ $receivedOrder, $shipment]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ReceivedOrder $receivedOrder, Shipment $shipment)
    {
        //
        return view('receivedOrders.shipments.show', compact('receivedOrder', 'shipment'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReceivedOrder $receivedOrder, Shipment $shipment)
    {
        //
        $shipment->delete();

        return redirect(route('receivedOrders.show', [$receivedOrder]));
    }
}
