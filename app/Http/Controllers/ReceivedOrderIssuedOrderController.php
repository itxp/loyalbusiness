<?php

namespace App\Http\Controllers;

use App\Carrier;
use App\Http\Requests\AddReceivedOrderIssuedOrderRequest;
use App\IssuedOrder;
use App\ReceivedOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReceivedOrderIssuedOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ReceivedOrder $receivedOrder)
    {
        //

        $carriers = Carrier::orderBy('name')->get();
        $lastNumber = IssuedOrder::max('number');
//        dd($lastNumber+2);

        return view('receivedOrders.issuedOrders.create', compact('carriers', 'receivedOrder', 'lastNumber'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReceivedOrder $receivedOrder, AddReceivedOrderIssuedOrderRequest $request)
    {
        $issuedOrder = $receivedOrder->issuedOrders()->create($request->all());

        return redirect()->route('receivedOrders.issuedOrders.show', [$receivedOrder->id, $issuedOrder->id]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ReceivedOrder $receivedOrder, IssuedOrder $issuedOrder)
    {
        //
        return view('receivedOrders.issuedOrders.show', compact('receivedOrder', 'issuedOrder'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReceivedOrder $receivedOrder, IssuedOrder $issuedOrder)
    {
        //
        $issuedOrder->delete();

        return redirect(route('receivedOrders.show', [$receivedOrder]));
    }
}
