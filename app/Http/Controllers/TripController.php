<?php

namespace App\Http\Controllers;

use App\Car;
use App\Driver;
use App\ExchangeRate;
use App\Http\Requests\AddTripRequest;
use App\Trip;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;


class TripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('trips.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $cars = Car::orderBy('plate')->get();
        $drivers = Driver::orderBy('name')->get();

        return view('trips.create', compact('cars', 'drivers'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddTripRequest $request)
    {
        //
        $trip = Trip::create($request->all());

        return redirect()->route('trips.show', [$trip]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    public function show(Trip $trip)
    {
        //
        $exchangeRate = ExchangeRate::whereDate('date','<=',$trip->getOriginal('date'))->orderBy('date','desc')->first();
        return view('trips.show', compact('trip', 'exchangeRate'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    public function edit(Trip $trip)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trip $trip)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trip $trip)
    {
        //
    }

    public function data(){

        $trips = Trip::join('cars', 'trips.car_id', 'cars.id')
            ->join('drivers', 'trips.driver_id', 'drivers.id')
            ->select('trips.id','cars.plate', 'drivers.name', 'trips.date');
//        dd($trips->get());
        return Datatables::of($trips)
            ->addColumn('action', function ($trip) {
//                dd($receivedOrder);
                return '<a href="'.route('trips.show', [$trip->id]).'" class="btn btn-xs btn"><i class="glyphicon glyphicon-file"></i> Deschide</a>';
            })
            ->filterColumn('date', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(trips.date,'%d/%m/%Y') like ?", ["%$keyword%"]);
            })
            ->make(true);

    }
}
