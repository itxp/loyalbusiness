<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddTripExpenseRequest;
use App\Trip;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class TripExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Trip $trip)
    {
        //
        return view('trips.expenses.index', compact('trip'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Trip $trip)
    {
        //
        return view('trips.expenses.create', compact('trip'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Trip $trip, AddTripExpenseRequest $request)
    {
        //

//        dd($request->all());
        $trip->expenses()->create($request->all());

        return redirect(route('trips.expenses.create', [$trip->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function data(Trip $trip)
    {
        //
        $expenses = $trip->expenses();

        return Datatables::of($expenses)
            ->filterColumn('date', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(expenses.date,'%d/%m/%Y') like ?", ["%$keyword%"]);
            })
            ->filterColumn('expiry_date', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(expenses.expiry_date,'%d/%m/%Y') like ?", ["%$keyword%"]);
            })
            ->addColumn('action', function ($expense) use ($trip)  {
                return '<a href="'.route('trips.expenses.edit', [$trip->id, $expense->id]).'" class="btn btn-xs btn"><i class="glyphicon glyphicon-edit"></i> Modifica</a>';
            })
            ->make(true);
    }
}
