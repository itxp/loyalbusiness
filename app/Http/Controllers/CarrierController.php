<?php

namespace App\Http\Controllers;

use App\Carrier;
use App\Country;
use App\Http\Requests\AddCarrierRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class CarrierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('carriers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $countries = Country::orderBy('name')->get();

        return view('carriers.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddCarrierRequest $request)
    {
        //
        $client = Carrier::create($request->all());

        return redirect(route('carriers.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Carrier  $carrier
     * @return \Illuminate\Http\Response
     */
    public function show(Carrier $carrier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Carrier  $carrier
     * @return \Illuminate\Http\Response
     */
    public function edit(Carrier $carrier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Carrier  $carrier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Carrier $carrier)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Carrier  $carrier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Carrier $carrier)
    {
        //
    }

    public function data()
    {
        //
//        $expenses = DB::table('expenses')->join('cars', 'expenses.car_id', 'cars.id')->select('cars.plate', 'expenses.name', 'expenses.date', 'expenses.cost');

//        $expenses = DB::table('expenses')->join('cars', 'expenses.car_id', 'cars.id')->select('cars.plate', 'expenses.name', 'expenses.date', 'expenses.cost');

        $carriers = DB::table('carriers')
            ->join('countries', 'carriers.country_id', 'countries.id')
            ->select('carriers.name', 'carriers.cif', 'countries.name as country', 'carriers.phone');

//            dd($clients->get());

        return Datatables::of($carriers)
            ->make(true);
    }
}
