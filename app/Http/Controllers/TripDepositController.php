<?php

namespace App\Http\Controllers;

use App\Trip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class TripDepositController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Trip $trip)
    {
        //
        return view('trips.deposits.index', compact('trip'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function depositsData(Trip $trip){

        $shipments = $trip
            ->deposits();

        return Datatables::of($shipments)
//            ->addColumn('action', function ($shipment) {
//                return '<a href="'.route('receivedOrders.shipments.show', [ $shipment->received_order_id, $shipment->id ]).'" class="btn btn-xs btn"><i class="glyphicon glyphicon-file"></i> Deschide</a>';
//            })
            ->make(true);

    }

    public function driverDepositsData(Trip $trip){

        $deposits = $trip
            ->driver
            ->deposits()
            ->whereNull('trip_id');

        return Datatables::of($deposits)

//            ->addColumn('action', function ($shipment) {
//                return '<a href="'.route('receivedOrders.shipments.show', [ $shipment->received_order_id, $shipment->id ]).'" class="btn btn-xs btn"><i class="glyphicon glyphicon-file"></i> Deschide</a>';
//            })
            ->make(true);

    }

    public function attachDeposits($trip_id, Request $request){

        $deposits = $request->input('shipments');
        if($deposits)
            DB::table('deposits')
                ->whereIn('id', $deposits)
                ->update(['trip_id' => $trip_id]);
        return 'ok';
    }

    public function detachDeposits($trip_id, Request $request){

        $deposits = $request->input('shipments');
        if($deposits)
            DB::table('deposits')
                ->whereIn('id', $deposits)
                ->update(['trip_id' => null]);
        return 'ok';
    }
}
