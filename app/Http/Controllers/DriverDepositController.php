<?php

namespace App\Http\Controllers;

use App\Deposit;
use App\Driver;
use App\Http\Requests\AddDriverDepositRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;


class DriverDepositController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Driver $driver)
    {
        //
        return view('drivers.deposits.index', compact('driver'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Driver $driver)
    {
        //
        return view('drivers.deposits.create', compact('driver'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddDriverDepositRequest $request, Driver $driver)
    {
        //
        $driver->deposits()->create($request->all());

        return redirect()->route('drivers.deposits.index', [$driver]);


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Deposit  $deposit
     * @return \Illuminate\Http\Response
     */
    public function show(Deposit $deposit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Deposit  $deposit
     * @return \Illuminate\Http\Response
     */
    public function edit(Deposit $deposit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Deposit  $deposit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Deposit $deposit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Deposit  $deposit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Deposit $deposit)
    {
        //
    }

    public function data(Driver $driver)
    {
        //
        $deposits = $driver->deposits();

        return Datatables::of($deposits)
            ->filterColumn('date', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(expenses.date,'%d/%m/%Y') like ?", ["%$keyword%"]);
            })
//            ->addColumn('action', function ($expense) use ($car)  {
//                $return = '<a href="'.route('cars.expenses.edit', [$car->id, $expense->id]).'" class="btn btn-xs btn"><i class="glyphicon glyphicon-edit"></i> Modifica</a>';
//                if((!$expense->expired)&&($expense->expiry_odometer||$expense->expiry_date))$return=$return.'<a href="'.route('cars.expenses.verified', [$car->id, $expense->id]).'" class="btn btn-xs btn"><i class="glyphicon glyphicon-ok"></i> Verificat</a>';
//                return $return;
//            })
            ->make(true);
    }
}
