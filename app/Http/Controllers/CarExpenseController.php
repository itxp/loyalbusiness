<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Car;
use App\Expense;
use App\Http\Requests\AddCarExpenseRequest;
use App\Http\Requests\EditCarExpenseRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;


class CarExpenseController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Car $car)
    {
        //
        return view('cars.expenses.index', compact('car'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Car $car)
    {
        //
//        dd(1);
        return view('cars.expenses.create', compact('car'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Car $car, AddCarExpenseRequest $request)
    {
        //
//        $request->all()['user_id']=Auth::user()->id;
//        dd($request->all());
        $car->expenses()->create($request->all());

//        return redirect(route('cars.createExpense', [$car->id]));

        return redirect(route('cars.expenses.create', [$car->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car, Expense $expense)
    {
        //

        return view('cars.expenses.edit', compact('car','expense'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditCarExpenseRequest $request, Car $car, Expense $expense)
    {
        //
        $expense->update($request->all());
        return redirect()->route('cars.expenses.index', [$car->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function data(Car $car)
    {
        //
//        dd($car->expenses());
        $expenses = $car->expenses();

        return Datatables::of($expenses)
            ->filterColumn('date', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(expenses.date,'%d/%m/%Y') like ?", ["%$keyword%"]);
            })
            ->filterColumn('expiry_date', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(expenses.expiry_date,'%d/%m/%Y') like ?", ["%$keyword%"]);
            })
            ->addColumn('action', function ($expense) use ($car)  {
                $return = '<a href="'.route('cars.expenses.edit', [$car->id, $expense->id]).'" class="btn btn-xs btn"><i class="glyphicon glyphicon-edit"></i> Modifica</a>';
                if((!$expense->expired)&&($expense->expiry_odometer||$expense->expiry_date))$return=$return.'<a href="'.route('cars.expenses.verified', [$car->id, $expense->id]).'" class="btn btn-xs btn"><i class="glyphicon glyphicon-ok"></i> Verificat</a>';
                return $return;
            })
            ->make(true);
    }

    public function verified(Car $car, Expense $expense)
    {
        //
        $expense->increment('expired');
        return redirect()->back();

    }

    public function all()
    {
        //
        return view('cars.expenses');

    }

    public function dataAll()
    {
        //
        $expenses = Expense::join('cars', 'expenses.car_id', 'cars.id')

            ->select('cars.plate', 'expenses.name', 'expenses.date', 'expenses.cost', 'expenses.currency', 'expenses.expiry_date')
            ->selectRaw('(expenses.expiry_odometer - cars.km) as odometer');

//        dd($expenses->get()[4]->expiry_date);
//        dd($expenses->get());
        Carbon::setLocale('ro');
//        dd(Carbon::createFromFormat('d/m/Y',$expenses->get()[4]->expiry_date)->diffForHumans(now()));
//        { data: 'plate', name: 'plate' },
//        { data: 'name', name: 'name' },
//        { data: 'date', name: 'date' },
//        { data: 'cost', name: 'cost' },

        return Datatables::of($expenses)
//            ->addColumn('action', function ($car) {
//                return '<a href="'.route('cars.expenses.index', [$car->id]).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Cheltuieli</a>
//                        <a href="'.route('cars.edit', [$car->id]).'" class="btn btn-xs btn"><i class="glyphicon glyphicon-edit"></i> Modifica</a>';
//            })
//            ->editColumn('expenses.expiry_date', function ($expense) {
//                return $expense->expiry_date?Carbon::createFromFormat('d/m/Y', $expense->expiry_date)->diffForHumans(now()):null;
//            })
//            ->editColumn('odometer', function ($expense) {
//                return $expense->expiry_odometer?$expense->expiry_odometer-$expense->km:null;
//            })
//            ->filterColumn('odometer', function ($query, $keyword) {
//                $query->whereRaw("(expenses.expiry_odometer - cars.km) like ?", ["%$keyword%"]);
//            })
//            ->editColumn('expenses.date', function ($expense) {
//                return $expense->date->format('d/m/Y');
//            })
            ->filterColumn('date', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(expenses.date,'%d/%m/%Y') like ?", ["%$keyword%"]);
            })
            ->make(true);
    }


}
