<?php

namespace App\Http\Controllers;

use App\Car;
use App\Device;
use App\Expense;
use App\IssuedOrder;
use App\Mail\Dev;
use App\Mail\Expire30Days;
use App\Mail\Expire7Days;
use App\Mail\ExpiredDate;
use App\Mail\ExpiredOdometer;
use App\Mail\ExpireOdometer;
use App\ReceivedOrder;
use App\Shipment;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class DevController extends Controller
{
    //
    public function mail()
    {
//        config(['settings.kkt' => 'bla']);
        dd(config('settings.vat'));
        $user = User::first();
        Mail::to('raul@itxp.ro')->send(new Dev($user));
    }



    public function db2(){

//        $devices = DB::connection('mysql2')->table('devices')
//            ->leftJoin('positions', 'devices.id', '=', 'positions.deviceid')
//            ->select('devices.name', 'positions.latitude', 'positions.longitude', 'positions.address')
//            ->get();

//        $car = Car::where('plate', 'AG01CO')->firstOrFail();
//
//        dd($car);



        $devices = Device::with('position')->get();
//dd($devices);
        foreach ($devices as $device){
            $car = Car::where('plate', $device->name)->first();

            if($car){
                if($device->position){
//                    $car->gps_id = $device->id;
                    $car->lat = $device->position->latitude;
                    $car->long = $device->position->longitude;
                    $car->km = round(json_decode($device->position->attributes, TRUE)['totalDistance']/1000);
                    $car->address = $device->position->address;
                    $car->save();
                }
            }

        }
//
//            }
//            else{
//                $car = new Car;
//                $car->plate = $device->name;
//                $car->save();
//            };
//        }
    }

    public function api()
    {
//        Bugsnag::notifyError('ErrorType', 'Test Error');
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'http://traccar.loialbusiness.ro:8082/api/positions', [
            'auth' => ['office@loialbusiness.ro', '0735445299']
        ]);
        $positions = json_decode($res->getBody(), true) ;

//        dd($positions);

        foreach ($positions as $position){
//            echo $position['deviceId'];
            $car = Car::where('gps_id', $position['deviceId'])->first();
//            dd($car);
            if($car){
                $car->lat = $position['latitude'];
                $car->long = $position['longitude'];
                $car->km = round($position['attributes']['totalDistance']/1000);
                $car->address = $position['address'];
                $car->save();
            }
//            dd($position['deviceId']);
        };

//        if($car){
//            if($device->position){
//                $car->lat = $device->position->latitude;
//                $car->long = $device->position->longitude;
//                $car->km = round(json_decode($device->position->attributes, TRUE)['totalDistance']/1000);
//                $car->address = $device->position->address;
//                $car->save();
//            }
//        }
    }

    public function test(){

        $expenses = DB::table('expenses')
            ->join('cars', 'expenses.car_id', 'cars.id')
            ->where('expired', false)
            ->where('expenses.notification_odometer', 0)
            ->whereRaw('(expenses.expiry_odometer - cars.km) <= 5000')
            ->select('expenses.name', 'cars.plate', 'expenses.expiry_odometer', 'cars.km')
            ->get();

        if($expenses->isNotEmpty()){
            Mail::to('raul@itxp.ro')->send(new ExpireOdometer($expenses));
            $expenses = DB::table('expenses')
                ->join('cars', 'expenses.car_id', 'cars.id')
                ->where('expired', false)
                ->where('expenses.notification_odometer', 0)
                ->whereRaw('(expenses.expiry_odometer - cars.km) <= 5000')
                ->select('expenses.name', 'cars.plate', 'expenses.expiry_odometer', 'cars.km')
                ->increment('expenses.notification_odometer');
        }

        $expenses = DB::table('expenses')
            ->join('cars', 'expenses.car_id', 'cars.id')
            ->where('expired', false)
            ->where('expenses.notification_odometer', 1)
            ->whereRaw('expenses.expiry_odometer <= cars.km')
            ->select('expenses.name', 'cars.plate', 'expenses.expiry_odometer', 'cars.km')
            ->get();

        if($expenses->isNotEmpty()){
            Mail::to('raul@itxp.ro')->send(new ExpiredOdometer($expenses));
            $expenses = DB::table('expenses')
                ->join('cars', 'expenses.car_id', 'cars.id')
                ->where('expired', false)
                ->where('expenses.notification_odometer', 1)
                ->whereRaw('expenses.expiry_odometer <= cars.km')
                ->select('expenses.name', 'cars.plate', 'expenses.expiry_odometer', 'cars.km')
                ->increment('expenses.expired');
        }

        $expenses = Expense::valid()->expiryDate()->where('notification_date', 0)->where('expiry_date', '<=', Carbon::now()->addDays(30))->with('car')->get();

        if($expenses->isNotEmpty()){
            Mail::to('raul@itxp.ro')->send(new Expire30Days($expenses));
            Expense::valid()->expiryDate()->where('notification_date', 0)->where('expiry_date', '<=', Carbon::now()->addDays(30))->increment('notification_date');
        }

        $expenses = Expense::valid()->expiryDate()->where('notification_date', 1)->where('expiry_date', '<=', Carbon::now()->addDays(7))->with('car')->get();

        if($expenses->isNotEmpty()){
            Mail::to('raul@itxp.ro')->send(new Expire7Days($expenses));
            Expense::valid()->expiryDate()->where('notification_date', 1)->where('expiry_date', '<=', Carbon::now()->addDays(7))->increment('notification_date');
        }

        $expenses = Expense::valid()->expiryDate()->where('notification_date', 2)->where('expiry_date', '<=', Carbon::now())->with('car')->get();

        if($expenses->isNotEmpty()){
            Mail::to('raul@itxp.ro')->send(new ExpiredDate($expenses));
            Expense::valid()->expiryDate()->where('notification_date', 2)->where('expiry_date', '<=', Carbon::now())->with('car')->increment('expired');
        }

//        dd($pending);

    }

    public function rate(){
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'http://www.bnr.ro/nbrfxrates.xml');
        $xml = new \SimpleXMLElement($res->getBody());
//        dd((string)$xml->Body->Cube->attributes()->date);
        dd((double)$xml->Body->Cube->Rate[10]);
        $positions = json_decode($res->getBody(), true) ;
        dd($positions);
        foreach ($positions as $position){
            $car = Car::where('gps_id', $position['deviceId'])->first();
            if($car){
                $car->lat = $position['latitude'];
                $car->long = $position['longitude'];
                $car->km = round($position['attributes']['totalDistance']/1000);
                $car->address = $position['address'];
                $car->save();
            }
        };
    }

    public function form(){
        return view('form');
    }

    public function post(Request $request){
        dd($request->all());
    }

    public function pdf(){
//        return view('pdf.invoice');
        $expence = Expense::orderBy('id','desc')->first();
//        dd($expence->date->format('d/m/Y'));
        $data = ['app' => 'lbm', 'type' => 'pdf'];

        $pdf = PDF::loadView('pdf.invoice', compact('data'))->setPaper('a4', 'portrait');
        return $pdf->stream();
        return $pdf->download('invoice.pdf');
    }

    public function issuedOrders(){
        dd(IssuedOrder::all());
    }

    public function test2(){

        ReceivedOrder::find(19)->update(['invoice_id' => null]);
    }
}
