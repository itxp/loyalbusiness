<?php

namespace App\Http\Controllers;

use App\Client;
use App\ExchangeRate;
use App\Http\Requests\AddInvoiceRequest;
use App\Invoice;
use App\Option;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Barryvdh\DomPDF\Facade as PDF;


class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('invoices.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $clients = Client::orderBy('name')->get();
        $exchangeRate = ExchangeRate::whereDate('date', '<=', Carbon::now())->orderBy('date','desc')->first();
//dd($exchangeRate);
        return view('invoices.create', compact('clients', 'exchangeRate', 'numbers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddInvoiceRequest $request)
    {
        //
//        dd($request->all());
        $invoice = Invoice::create($request->all());
//        dd($invoice->date);
        $client = Client::findOrFail($invoice->client_id);
        $buyer = $client->toArray();
        $buyer['country'] = $client->country->name;

        $invoice->buyer = $buyer;
//        $invoice->supplier = 'kkt';

//        dd($invoice);
        $invoice->supplier = Option::where('key','header')->first()->value;
//        dd($invoice);
        $invoice->update();
        return redirect(route('invoices.show', [$invoice->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        //
//        dd($invoice->buyer['name']);
//$client = $invoice->client;
//        dd($invoice->client);
        return view('invoices.show', compact('invoice'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        //
    }

    public function maxNumber($series){
        return Invoice::where('series', $series)->max('number')+1;
    }

    public function exchangeRate($day, $month, $year){
//        dd();
        return ExchangeRate::whereDate('date', '<=', $year.'-'.$month.'-'.$day)->orderBy('date','desc')->first();

    }

    public function data(){

        $invoices = Invoice::join('clients', 'invoices.client_id', 'clients.id')
            ->select('invoices.id', 'invoices.series', 'invoices.number', 'clients.name', 'invoices.date', 'invoices.total_ron', 'invoices.total_eur');

        return Datatables::of($invoices)
            ->addColumn('action', function ($invoice) {
//                dd($receivedOrder);
                return '<a href="'.route('invoices.show', [$invoice->id]).'" class="btn btn-xs btn"><i class="glyphicon glyphicon-file"></i> Deschide</a>';
            })
            ->filterColumn('date', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(invoices.date,'%d/%m/%Y') like ?", ["%$keyword%"]);
            })
            ->make(true);

    }

    public function pdfOpen(Invoice $invoice)
    {
        $header = Option::where('key','header')->first()->value;
//        return view('invoices.pdf', compact('invoice','header'));
        $pdf = PDF::loadView('invoices.pdf', compact('invoice','header'))->setPaper('a4', 'portrait');
        return $pdf->stream();
    }

    public function pdfDownload(Invoice $invoice)
    {
        $header = Option::where('key','header')->first()->value;
        $pdf = PDF::loadView('invoices.pdf', compact('invoice','header'))->setPaper('a4', 'portrait');
        return $pdf->download('factura-'.$invoice->series.'#'.$invoice->number.'.pdf');
    }
}
