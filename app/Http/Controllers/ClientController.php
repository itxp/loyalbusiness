<?php

namespace App\Http\Controllers;

use App\Client;
use App\Country;
use App\Http\Requests\AddClientRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('clients.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $countries = Country::orderBy('name')->get();

        return view('clients.create', compact('countries'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddClientRequest $request)
    {
        //
//dd($request->all());
        $client = Client::create($request->all());

        if($client->country->iso == 'RO'){
            $client->vat = 19;
            $client->save();
        }

//        dd($client->country->iso);

        return redirect(route('clients.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
        dd($client);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        //
    }

    public function data()
    {
        //
//        $expenses = DB::table('expenses')->join('cars', 'expenses.car_id', 'cars.id')->select('cars.plate', 'expenses.name', 'expenses.date', 'expenses.cost');

//        $expenses = DB::table('expenses')->join('cars', 'expenses.car_id', 'cars.id')->select('cars.plate', 'expenses.name', 'expenses.date', 'expenses.cost');

        $clients = DB::table('clients')
            ->join('countries', 'clients.country_id', 'countries.id')
            ->select('clients.name', 'clients.cif', 'countries.name as country', 'clients.phone');

//            dd($clients->get());

        return Datatables::of($clients)
            ->make(true);
    }

    public function single(Client $client){
//        dd($client);
        return $client;
    }
}
