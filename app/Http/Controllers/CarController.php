<?php

namespace App\Http\Controllers;

use App\Car;
use App\Http\Requests\AddCarRequest;
use App\Http\Requests\EditCarRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class CarController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('cars.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('cars.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddCarRequest $request)
    {
        //

//        dd($request);

        Car::create($request->all());

        return redirect(route('cars.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        //
        dd($car);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car)
    {
        //
        return view('cars.edit', compact('car'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function update(EditCarRequest $request, Car $car)
    {
        //
//        dd($car);
        $car->update($request->all());
        return redirect()->route('cars.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function data()
    {
        //
        return Datatables::of(Car::query())
            ->addColumn('action', function ($car) {
                return '<a href="'.route('cars.expenses.create', [$car->id]).'" class="btn btn-xs btn"><i class="glyphicon glyphicon-plus"></i> Adauga cheltuiala</a>
                        <a href="'.route('cars.expenses.index', [$car->id]).'" class="btn btn-xs btn-primary">Cheltuieli</a>
                        <a href="'.route('cars.edit', [$car->id]).'" class="btn btn-xs btn"><i class="glyphicon glyphicon-edit"></i> Modifica</a>';
            })
            ->make(true);
    }




}
