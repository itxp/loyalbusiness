<?php

namespace App\Http\Controllers;

use App\Client;
use App\Http\Requests\AddReceivedOrderRequest;
use App\ReceivedOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Models\Activity;
use Yajra\DataTables\DataTables;

class ReceivedOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('receivedOrders.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $clients = Client::orderBy('name')->get();

        return view('receivedOrders.create', compact('clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddReceivedOrderRequest $request)
    {
        //
        $receivedOrder = ReceivedOrder::create($request->all());

        return redirect()->route('receivedOrders.show', [$receivedOrder->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReceivedOrder  $receivedOrder
     * @return \Illuminate\Http\Response
     */
    public function show(ReceivedOrder $receivedOrder)
    {
        //
        return view('receivedOrders.show', compact('receivedOrder'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReceivedOrder  $receivedOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(ReceivedOrder $receivedOrder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReceivedOrder  $receivedOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReceivedOrder $receivedOrder)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReceivedOrder  $receivedOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReceivedOrder $receivedOrder)
    {
        //
    }

    public function data(){

        $receivedOrders = ReceivedOrder::join('clients', 'received_orders.client_id', 'clients.id')
            ->with('invoice')
            ->select('received_orders.id','clients.name', 'received_orders.number', 'received_orders.date', 'received_orders.price', 'received_orders.currency', 'received_orders.invoice_id');
//        dd($receivedOrders->get());

        return Datatables::of($receivedOrders)
            ->addColumn('action', function ($receivedOrder) {
//                dd($receivedOrder);
                return '<a href="'.route('receivedOrders.show', [$receivedOrder->id]).'" class="btn btn-xs btn"><i class="glyphicon glyphicon-file"></i> Deschide</a>';
            })
            ->addColumn('invoice', function ($receivedOrder) {
                if(empty($receivedOrder->invoice_id))return 'Nefacturata';
                else return '<a href="'.route('invoices.show', [$receivedOrder->invoice->id]).'">' . $receivedOrder->invoice->series . ' ' . $receivedOrder->invoice->number . '</a>';
            })
            ->rawColumns(['invoice', 'action'])
            ->filterColumn('date', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(received_orders.date,'%d/%m/%Y') like ?", ["%$keyword%"]);
            })
            ->make(true);

    }

    public function log(ReceivedOrder $receivedOrder){
        dd($receivedOrder->activity);
    }
}
