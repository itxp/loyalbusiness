<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddInvoiceDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'description' => 'required',
            'unit' => 'required',
            'quantity' => 'required|numeric',
            'price_ron' => 'required|numeric',
            'price_eur' => 'required|numeric',
//            'price_ron' => 'required_without:price_eur|numeric',
//            'price_eur' => 'required_without:price_ron|numeric',
        ];
    }
}
