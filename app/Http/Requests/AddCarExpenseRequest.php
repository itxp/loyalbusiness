<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddCarExpenseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required',
            'cost' => 'required|numeric',
            'currency' => 'required',
            'date' => 'required|date_format:"d/m/Y"',
            'odometer' => 'required|numeric',
            'expiry_odometer' => 'sometimes|nullable|numeric',
            'expiry_date' => 'sometimes|nullable|date_format:"d/m/Y"'
        ];
    }
}
