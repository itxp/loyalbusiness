<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddInvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'series' => 'required',
            'number' => 'required|numeric',
            'date' => 'required|date_format:"d/m/Y"',
            'client_id' => 'required|numeric',
            'exchange_rate' => 'required|numeric',
            'vat' => 'required|numeric',
            'discount' => 'required|numeric',
            'due_date' => 'required|date_format:"d/m/Y"',
            'user_id' => 'required|numeric',
            'delegate' => 'array'
        ];
    }
}
