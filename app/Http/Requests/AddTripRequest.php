<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddTripRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'car_id' => 'required|numeric',
            'driver_id' => 'required|numeric',
            'date' => 'required|date_format:"d/m/Y"',
            'date_start' => 'required|date_format:"d/m/Y"',
            'date_end' => 'required|date_format:"d/m/Y"',
            'route_sheet' => 'required',
            'displacement_order' => 'required',
            'odometer_start' => 'required|numeric',
            'odometer_end' => 'required|numeric',
            'fuel' => 'required|numeric',
        ];
    }
}
