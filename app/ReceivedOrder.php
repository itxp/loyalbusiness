<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class ReceivedOrder extends Model
{
    //
    use LogsActivity;

    protected static $logAttributes = ['*'];

    protected $fillable = [
        'client_id',
        'number',
        'date',
        'opt',
        'price',
        'currency',
        'description',
        'user_id'
    ];

    public function setDateAttribute($value)
    {
        $date = \DateTime::createFromFormat('d/m/Y', $value);

        $this->attributes['date'] =  $date->format('Y-m-d H:i:s');
    }

    public function getDateAttribute($value)
    {
        $date = \DateTime::createFromFormat('Y-m-d', $value);

        return $date->format('d/m/Y');
    }

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    public function freights()
    {
        return $this->morphMany('App\Freight', 'freightable');
    }

    public function issuedOrders()
    {
        return $this->hasMany('App\IssuedOrder');
    }

    public function shipments()
    {
        return $this->hasMany('App\Shipment');
    }

    public function scopeNoInvoice($query)
    {
        return $query->whereNull('invoice_id');
    }

    public function invoice()
    {
        return $this->belongsTo('App\Invoice');
    }

    public function activity()
    {
        return $this->morphMany('Spatie\Activitylog\Models\Activity', 'subject');
    }

}
