<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class Car extends Model
{
    //
    use LogsActivity;

    protected static $logAttributes = ['*'];

    protected $fillable = [
        'plate',
        'description'
    ];

    public function expenses()
    {
        return $this->hasMany('App\Expense');
    }

    public function shipments()
    {
        return $this->hasMany('App\Shipment');
    }
}
