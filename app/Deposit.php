<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class Deposit extends Model
{
    //
    use LogsActivity;

    protected static $logAttributes = ['*'];

    protected $fillable = [
        'amount',
        'currency',
        'date',
        'description',
    ];

    public function setDateAttribute($value)
    {
        $date = \DateTime::createFromFormat('d/m/Y', $value);

        $this->attributes['date'] =  $date->format('Y-m-d');
    }

    public function getDateAttribute($value)
    {
        $date = \DateTime::createFromFormat('Y-m-d', $value);

        return $date->format('d/m/Y');
    }

    public function driver()
    {
        return $this->belongsTo('App\Driver');
    }

    public function scopeEur($query)
    {
        return $query->where('currency','EUR');
    }

    public function scopeRon($query)
    {
        return $query->where('currency','RON');
    }
}
