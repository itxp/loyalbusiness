<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class Trip extends Model
{
    //
    use LogsActivity;

    protected static $logAttributes = ['*'];

    protected $fillable = [
        'car_id',
        'driver_id',
        'route_sheet',
        'displacement_order',
        'odometer_start',
        'odometer_end',
        'date',
        'date_start',
        'date_end',
        'fuel'
    ];

    public function setDateAttribute($value)
    {
        $date = \DateTime::createFromFormat('d/m/Y', $value);

        $this->attributes['date'] =  $date->format('Y-m-d H:i:s');
    }

    public function getDateAttribute($value)
    {
        $date = \DateTime::createFromFormat('Y-m-d', $value);

        return $date->format('d/m/Y');
    }

    public function setDateStartAttribute($value)
    {
        $date = \DateTime::createFromFormat('d/m/Y', $value);

        $this->attributes['date_start'] =  $date->format('Y-m-d H:i:s');
    }

    public function getDateStartAttribute($value)
    {
        $date = \DateTime::createFromFormat('Y-m-d', $value);

        return $date->format('d/m/Y');
    }
    public function setDateEndAttribute($value)
    {
        $date = \DateTime::createFromFormat('d/m/Y', $value);

        $this->attributes['date_end'] =  $date->format('Y-m-d H:i:s');
    }

    public function getDateEndAttribute($value)
    {
        $date = \DateTime::createFromFormat('Y-m-d', $value);

        return $date->format('d/m/Y');
    }

    public function car()
    {
        return $this->belongsTo('App\Car');
    }

    public function driver()
    {
        return $this->belongsTo('App\Driver');
    }

    public function shipments()
    {
        return $this->hasMany('App\Shipment');
    }

    public function expenses()
    {
        return $this->hasMany('App\Expense');
    }

    public function deposits()
    {
        return $this->hasMany('App\Deposit');
    }
}
