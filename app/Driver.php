<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class Driver extends Model
{
    //
    use LogsActivity;

    protected static $logAttributes = ['*'];

    protected $fillable = [
        'name',
        'description'
    ];

    public function deposits()
    {
        return $this->hasMany('App\Deposit');
    }
}
