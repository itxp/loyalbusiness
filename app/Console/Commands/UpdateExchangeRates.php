<?php

namespace App\Console\Commands;

use App\ExchangeRate;
//use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateExchangeRates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:update-rates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'http://www.bnr.ro/nbrfxrates.xml');
        $xml = new \SimpleXMLElement($res->getBody());

        $exchangeRate = new ExchangeRate;
        $exchangeRate->rate = (double)$xml->Body->Cube->Rate[10];
        $exchangeRate->date = (string)$xml->Body->Cube->attributes()->date;
        $exchangeRate->save();

    }
}
