<?php

namespace App\Console\Commands;

use App\Expense;
use App\Mail\Expire30Days;
use App\Mail\Expire7Days;
use App\Mail\ExpiredDate;
use App\Mail\ExpiredOdometer;
use App\Mail\ExpireOdometer;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ExpenseNotification extends Command
{
    public $to = 'office@loialbusiness.ro';
    public $cc = 'raul@itxp.ro';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:expense-notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify pending expiration expenses and expired ones';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

//        dd($this->cc);

        $expenses = DB::table('expenses')
            ->join('cars', 'expenses.car_id', 'cars.id')
            ->where('expired', false)
            ->where('expenses.notification_odometer', 0)
            ->whereRaw('(expenses.expiry_odometer - cars.km) <= 5000')
            ->select('expenses.name', 'cars.plate', 'expenses.expiry_odometer', 'cars.km')
            ->get();

        if($expenses->isNotEmpty()){
            Mail::to($this->to)
                ->cc($this->cc)
                ->send(new ExpireOdometer($expenses));
            $expenses = DB::table('expenses')
                ->join('cars', 'expenses.car_id', 'cars.id')
                ->where('expired', false)
                ->where('expenses.notification_odometer', 0)
                ->whereRaw('(expenses.expiry_odometer - cars.km) <= 5000')
                ->select('expenses.name', 'cars.plate', 'expenses.expiry_odometer', 'cars.km')
                ->increment('expenses.notification_odometer');
        }

        $expenses = DB::table('expenses')
            ->join('cars', 'expenses.car_id', 'cars.id')
            ->where('expired', false)
            ->where('expenses.notification_odometer', 1)
            ->whereRaw('expenses.expiry_odometer <= cars.km')
            ->select('expenses.name', 'cars.plate', 'expenses.expiry_odometer', 'cars.km')
            ->get();

        if($expenses->isNotEmpty()){
            Mail::to($this->to)
                ->cc($this->cc)
                ->send(new ExpiredOdometer($expenses));
            $expenses = DB::table('expenses')
                ->join('cars', 'expenses.car_id', 'cars.id')
                ->where('expired', false)
                ->where('expenses.notification_odometer', 1)
                ->whereRaw('expenses.expiry_odometer <= cars.km')
                ->select('expenses.name', 'cars.plate', 'expenses.expiry_odometer', 'cars.km')
                ->increment('expenses.expired');
        }

        $expenses = Expense::valid()->expiryDate()->where('notification_date', 0)->where('expiry_date', '<=', Carbon::now()->addDays(30))->with('car')->get();

        if($expenses->isNotEmpty()){
            Mail::to($this->to)
                ->cc($this->cc)
                ->send(new Expire30Days($expenses));
            Expense::valid()->expiryDate()->where('notification_date', 0)->where('expiry_date', '<=', Carbon::now()->addDays(30))->increment('notification_date');
        }

        $expenses = Expense::valid()->expiryDate()->where('notification_date', 1)->where('expiry_date', '<=', Carbon::now()->addDays(7))->with('car')->get();

        if($expenses->isNotEmpty()){
            Mail::to($this->to)
                ->cc($this->cc)
                ->send(new Expire7Days($expenses));
            Expense::valid()->expiryDate()->where('notification_date', 1)->where('expiry_date', '<=', Carbon::now()->addDays(7))->increment('notification_date');
        }

        $expenses = Expense::valid()->expiryDate()->where('notification_date', 2)->where('expiry_date', '<=', Carbon::now())->with('car')->get();

        if($expenses->isNotEmpty()){
            Mail::to($this->to)
                ->cc($this->cc)
                ->send(new ExpiredDate($expenses));
            Expense::valid()->expiryDate()->where('notification_date', 2)->where('expiry_date', '<=', Carbon::now())->with('car')->increment('expired');
        }

    }
}
