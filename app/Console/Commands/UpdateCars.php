<?php

namespace App\Console\Commands;

use App\Car;
use Illuminate\Console\Command;

class UpdateCars extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:update-cars';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update cars from api to db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'http://traccar.loialbusiness.ro:8082/api/positions', [
            'auth' => ['office@loialbusiness.ro', '0735445299'],
            'headers' => [
                'Accept'     => 'application/json',
            ]
        ]);
        $positions = json_decode($res->getBody(), true) ;

        foreach ($positions as $position){
            $car = Car::where('gps_id', $position['deviceId'])->first();
            if($car){
                $car->lat = $position['latitude'];
                $car->long = $position['longitude'];
                $car->km = round($position['attributes']['totalDistance']/1000);
                $car->address = $position['address'];
                $car->save();
            }
        };
    }
}
