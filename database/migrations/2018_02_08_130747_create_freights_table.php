<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFreightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('freights', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference')->nullable();
            $table->string('freight')->nullable();
            $table->text('description')->nullable();
            $table->dateTime('load_datetime');
            $table->integer('load_country_id')->unsigned();
            $table->string('load_address')->nullable();
            $table->string('load_company')->nullable();
            $table->dateTime('unload_datetime');
            $table->integer('unload_country_id')->unsigned();
            $table->string('unload_address')->nullable();
            $table->string('unload_company')->nullable();
            $table->integer('freightable_id')->unsigned();
            $table->string('freightable_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('freights');
    }
}
