<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDetailsToTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trips', function (Blueprint $table) {
            //
            $table->integer('car_id')->unsigned();
            $table->integer('driver_id')->unsigned();
            $table->string('route_sheet')->nullable();
            $table->string('displacement_order')->nullable();
            $table->integer('odometer_start')->unsigned()->nullable();
            $table->integer('odometer_end')->unsigned()->nullable();
            $table->date('date')->nullable();
            $table->date('date_start')->nullable();
            $table->date('date_end')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trips', function (Blueprint $table) {
            //
            $table->dropColumn([
                'car_id',
                'driver_id',
                'route_sheet',
                'displacement_order',
                'odometer_start',
                'odometer_end',
                'date',
                'date_start',
                'date_end',
            ]);
        });
    }
}
