<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->text('supplier')->nullable();
            $table->string('series')->default('LBM');
            $table->integer('number')->unsigned();
            $table->date('date');
            $table->integer('client_id')->unsigned();
            $table->json('buyer')->nullable();
            $table->json('details')->nullable();
            $table->decimal('total_ron',10,2)->default(0);
            $table->decimal('total_eur',10,2)->default(0);
            $table->decimal('exchange_rate',6,4);
            $table->integer('vat')->unsigned()->default(0);
            $table->integer('discount')->unsigned()->default(0);
            $table->date('due_date');
            $table->json('delegate')->nullable();
            $table->boolean('paid')->default(false);
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            $table->unique(['series', 'number']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
