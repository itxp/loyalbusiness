<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('received_order_id')->unsigned();
            $table->integer('car_id')->unsigned();
            $table->integer('trip_id')->unsigned()->nullable();
            $table->integer('distance_loaded')->unsigned()->nullable();
            $table->integer('distance_unloaded')->unsigned()->nullable();
            $table->decimal('price',10,2)->default(0);
            $table->string('currency', 3)->default('EUR');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipments');
    }
}
