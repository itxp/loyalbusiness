<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeTosDriverAndPlateToIssuedOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('issued_orders', function (Blueprint $table) {
            //
            $table->string('type')->nullable();
            $table->string('plate')->nullable();
            $table->string('driver')->nullable();
            $table->text('tos')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('issued_orders', function (Blueprint $table) {
            //
            $table->dropColumn('type');
            $table->dropColumn('plate');
            $table->dropColumn('driver');
            $table->dropColumn('tos');
        });
    }
}
