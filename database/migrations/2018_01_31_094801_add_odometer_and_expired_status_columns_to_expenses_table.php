<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOdometerAndExpiredStatusColumnsToExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expenses', function (Blueprint $table) {
            //
            $table->integer('odometer')->unsigned()->nullable();
            $table->boolean('expired')->default(false);
            $table->integer('notification_date')->unsigned()->default(0);
            $table->integer('notification_odometer')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expenses', function (Blueprint $table) {
            //
            $table->dropColumn('odometer');
            $table->dropColumn('expired');
            $table->dropColumn('notification_date');
            $table->dropColumn('notification_odometer');
        });
    }
}
