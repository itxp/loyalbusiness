<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('cif');
            $table->string('orc')->nullable();
            $table->integer('country_id')->unsigned();
            $table->string('address')->nullable();
            $table->string('bank')->nullable();
            $table->string('iban')->nullable();
            $table->string('swift')->nullable();
            $table->string('phone')->nullable();
            $table->string('contact')->nullable();
            $table->integer('discount')->default(0);
//            $table->boolean('')->default(false);
            $table->integer('vat')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
