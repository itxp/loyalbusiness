<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssuedOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issued_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('received_order_id');
            $table->integer('carrier_id')->unsigned();
            $table->date('date');
            $table->decimal('price',10,2)->default(0);
            $table->text('description')->nullable();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issued_orders');
    }
}
