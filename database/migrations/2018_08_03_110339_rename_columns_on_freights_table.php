<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnsOnFreightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('freights', function (Blueprint $table) {
            //
            $table->renameColumn('load_datetime', 'load_date');
            $table->renameColumn('load_hours', 'load_mentions');
            $table->renameColumn('unload_datetime', 'unload_date');
            $table->renameColumn('unload_hours', 'unload_mentions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('freights', function (Blueprint $table) {
            //
            $table->renameColumn('load_date', 'load_datetime');
            $table->renameColumn('load_mentions', 'load_hours');
            $table->renameColumn('unload_date', 'unload_datetime');
            $table->renameColumn('unload_mentions', 'unload_hours');
        });
    }
}
