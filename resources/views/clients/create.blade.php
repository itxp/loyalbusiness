@extends('layouts.main')

@section('head-inject')
  <!-- DataTables -->
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Flota
      <small>Cheltuiala noua</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">General Elements</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Cheltuiala noua</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="POST" action="{{ route('clients.store') }}">

            {{ csrf_field() }}

            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">

            <div class="box-body">
              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif
              <div class="form-group">
                <label for="inputName">Nume*</label>
                <input type="text" class="form-control" id="inputName" placeholder="Exemplu SRL" value="{{ old('name') }}" name="name">
              </div>
              <div class="form-group">
                <label for="inputCIF">CIF*</label>
                <input type="text" class="form-control" id="inputCIF" placeholder="DE-123456 / 123123 / RO456456" value="{{ old('cif') }}" name="cif">
              </div>
              <div class="form-group">
                <label for="inputORC">Numar Registrul Comertului</label>
                <input type="text" class="form-control" id="inputORC" placeholder="J03/456/2008" value="{{ old('orc') }}" name="orc">
              </div>
              <div class="form-group">
                <label for="inputCountry">Tara</label>
                <select class="form-control" id="inputCountry" name="country_id">
                  @foreach($countries as $country)
                    <option value="{{ $country->id }}"@if(old('country_id')==$country->id) selected @endif>{{ $country->name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="inputAddress">Adresa</label>
                <input type="text" class="form-control" id="inputAddress" placeholder="Koln 65432, Strase XXX, no. 5" value="{{ old('address') }}" name="address">
              </div>
              <div class="form-group">
                <label for="inputBank">Banca</label>
                <input type="text" class="form-control" id="inputBank" placeholder="BRD" value="{{ old('bank') }}" name="bank">
              </div>
              <div class="form-group">
                <label for="inputIBAN">IBAN</label>
                <input type="text" class="form-control" id="inputIBAN" placeholder="ROXX YYYY ZZZZ ZZZZ ZZZZ ZZZZ" value="{{ old('iban') }}" name="iban">
              </div>
              <div class="form-group">
                <label for="inputSWIFT">SWIFT / BIC</label>
                <input type="text" class="form-control" id="inputSWIFT" placeholder="INGBROBU" value="{{ old('swift') }}" name="swift">
              </div>
              <div class="form-group">
                <label for="inputPhone">Telefon</label>
                <input type="text" class="form-control" id="inputPhone" placeholder="+34 123 123 123" value="{{ old('phone') }}" name="phone">
              </div>
              <div class="form-group">
                <label for="inputEmail">Email</label>
                <input type="email" class="form-control" id="inputEmail" placeholder="Email" value="{{ old('email') }}" name="email">
              </div>
              <div class="form-group">
                <label for="inputContact">Contact</label>
                <input type="text" class="form-control" id="inputContact" placeholder="Klaus George" value="{{ old('contact') }}" name="contact">
              </div>
              <div class="form-group">
                <label for="inputDiscount">Discount %</label>
                <input type="number" class="form-control" id="inputDiscount" placeholder="3" value="{{ old('discount')?old('discount'):0 }}" name="discount">
              </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Salveaza</button>
            </div>
          </form>
        </div>
      </div>
        <!-- /.box -->

    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

@endsection

@push('css')

  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

@endpush

@push('scripts')

  <!-- bootstrap datepicker -->
  <script src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script>
      //Date picker
      $('#expiryDate').datepicker({
          format: 'yyyy/mm/dd',
          autoclose: true
      })
      //Date picker
      $('#date').datepicker({
          format: 'yyyy/mm/dd',
          autoclose: true
      })
  </script>

@endpush