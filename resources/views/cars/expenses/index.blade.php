@extends('layouts.main')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endpush

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Flota
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Cheltuieli {{ $car->plate }}</h3>

            <div>
                <a href="{{ route('cars.expenses.create', [$car->id]) }}" class="btn btn-success">Adauga cheltuiala</a>

            </div>

          </div>
          <!-- /.box-header -->
          <div class="box-body" style="white-space:nowrap;">


            <table class="table table-bordered" id="cars-table" width="100%">
              <thead>
              <tr>
                  <th>Nume</th>
                  <th>Data</th>
                  <th>Cost</th>
                  <th>Moneda</th>
                  <th>Expira la data</th>
                  <th>Expira la km</th>
                  <th>Actiune</th>
              </tr>
              </thead>
            </table>

          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

@endsection

@push('scripts')

  <!-- DataTables -->
  <script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

  <!-- page script -->
  {{--<script>--}}
      {{--$(function () {--}}
          {{--// $('#example1').DataTable()--}}
          {{--$('#example1').DataTable({--}}
              {{--'paging'      : true,--}}
              {{--'lengthChange': false,--}}
              {{--// 'searching'   : false,--}}
              {{--// "columns": [--}}
              {{--//     { "searchable": false },--}}
              {{--//     null,--}}
              {{--//     null,--}}
              {{--//     null,--}}
              {{--//     null--}}
              {{--// ],--}}
              {{--'ordering'    : true,--}}
              {{--'info'        : true,--}}
              {{--'autoWidth'   : false,--}}
              {{--// 'filtering'   : true--}}
          {{--})--}}
      {{--})--}}
  {{--</script>--}}

  <script>
      $(function() {
          $('#cars-table').DataTable({
              // responsive: true,
              processing: true,
              serverSide: true,
              ajax: '{!! route('data.cars.expenses', [$car->id]) !!}',
              columns: [
                  { data: 'name', name: 'name' },
                  { data: 'date', name: 'date' },
                  { data: 'cost', name: 'cost' },
                  { data: 'currency', name: 'currency' },
                  { data: 'expiry_date', name: 'expiry_date' },
                  { data: 'expiry_odometer', name: 'expiry_odometer' },
                  { data: 'action', name: 'action', orderable: false, searchable: false}

              ],
              "scrollX": true,
              "order": [[ 1, "desc" ]],
              // columnDefs: [
              //     { responsivePriority: 1, targets: 0 },
              //     { responsivePriority: 2, targets: -1 }
              // ]
          });
      });
  </script>

@endpush