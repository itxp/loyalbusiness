<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 0.0.1 beta
    </div>
    <strong>Copyright &copy; 2018 <a href="http://itxp.ro">itxp.ro</a>.</strong> Toate drepturile rezervate.
</footer>