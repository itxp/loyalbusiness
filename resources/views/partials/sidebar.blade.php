<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/dist/img/user2-160x160.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Navigatie Principala</li>
            {{--<li class="active treeview">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-dashboard"></i> <span>Tablou de bord</span>--}}
                    {{--<span class="pull-right-container">--}}
                    {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                {{--<li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>--}}
                {{--<li><a href="/index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-download fa-fw" aria-hidden="true"></i> <span>Comenzi primite</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('receivedOrders.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Adauga comanda primita</a></li>
                    <li><a href="{{ route('receivedOrders.index') }}"><i class="fa fa-list-alt" aria-hidden="true"></i> Lista comenzi primite</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-upload fa-fw" aria-hidden="true"></i> <span>Comenzi emise</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('issuedOrders.index') }}"><i class="fa fa-list-alt" aria-hidden="true"></i> Lista comenzi emise</a></li>

                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-book fa-fw" aria-hidden="true"></i> <span>Deconturi</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('trips.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Decont nou</a></li>
                    <li><a href="{{ route('trips.index') }}"><i class="fa fa-list-alt" aria-hidden="true"></i> Lista deconturi</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-car fa-fw" aria-hidden="true"></i> <span>Flota</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('cars.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Adauga masina</a></li>
                    <li><a href="{{ route('cars.index') }}"><i class="fa fa-list-alt" aria-hidden="true"></i> Lista masini</a></li>
                    <li><a href="{{ route('cars.expenses') }}"><i class="fa fa-list-alt" aria-hidden="true"></i> Lista cheltuieli</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-money fa-fw" aria-hidden="true"></i> <span>Cheltuieli</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('expenses.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Adauga cheltuiala</a></li>
                    <li><a href="{{ route('expenses.index') }}"><i class="fa fa-list-alt" aria-hidden="true"></i> Lista cheltuieli</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-male fa-fw" aria-hidden="true"></i>
                    <span>Soferi</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('drivers.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Adauga sofer</a></li>
                    <li><a href="{{ route('drivers.index') }}"><i class="fa fa-list-alt" aria-hidden="true"></i> Lista soferi</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-file-text fa-fw" aria-hidden="true"></i> <span>Facturi</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('invoices.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Factura noua</a></li>
                    <li><a href="{{ route('invoices.index') }}"><i class="fa fa-list-alt" aria-hidden="true"></i> Lista facturi</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-address-card fa-fw" aria-hidden="true"></i>
                    <span>Clienti</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('clients.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Adauga client</a></li>
                    <li><a href="{{ route('clients.index') }}"><i class="fa fa-list-alt" aria-hidden="true"></i> Lista clienti</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-truck fa-fw" aria-hidden="true"></i>
                    <span>Transportatori</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('carriers.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> Adauga transportator</a></li>
                    <li><a href="{{ route('carriers.index') }}"><i class="fa fa-list-alt" aria-hidden="true"></i> Lista transportatori</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gear fa-fw" aria-hidden="true"></i> <span>Setari</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    <li class="treeview">
                        <a href="#"><i class="fa fa-globe"></i> Tari
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            {{--<li><a href="#"><i class="fa fa-plus"></i> Adauga tara</a></li>--}}
                            <li><a href="{{ route('settings.countries.index') }}"><i class="fa fa-list-alt"></i> Lista tari</a></li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#"><i class="fa fa-user"></i> Useri
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ route('settings.users.create') }}"><i class="fa fa-plus"></i> Adauga user</a></li>
                            <li><a href="{{ route('settings.users.index') }}"><i class="fa fa-list-alt"></i> Lista useri</a></li>
                        </ul>
                    </li>


                </ul>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>