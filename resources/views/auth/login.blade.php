<!DOCTYPE html>
<html>

  @include('partials.head')

  <!-- iCheck -->
  <link rel="stylesheet" href="/plugins/iCheck/square/blue.css" type="text/css">

<body class="hold-transition login-page login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="/index2.html"><b>Admin</b>LBM</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg"></p>

    <form method="POST" action="{{ route('login') }}">

      {{ csrf_field() }}

      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email"  name="email" value="{{ old('email') }}" required autofocus>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Parola" name="password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        {{--<div class="col-xs-8">--}}
          {{--<div class="checkbox icheck">--}}
            {{--<label>--}}
              {{--<input type="checkbox"> Tine-ma minte--}}
            {{--</label>--}}
          {{--</div>--}}
        {{--</div>--}}
        <!-- /.col -->
        <div class="col-xs-6 col-lg-offset-3">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Intra</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    {{--<a href="#">Am uitat parola</a><br>--}}
    {{--<a href="register.html" class="text-center">Register a new membership</a>--}}

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>

{{--@push('css')--}}



{{--@endpush--}}
