@component('mail::message')
# Expira sub 5000km

<ol>
@foreach($expenses as $expense)
     <li><b>{{ $expense->name }}</b> pentru <b>{{ $expense->plate }}</b> in aproximativ <b>{{ $expense->expiry_odometer - $expense->km }} km</b></li>
@endforeach
</ol>

{{ config('app.name') }}
@endcomponent
