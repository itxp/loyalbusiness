@component('mail::message')
# Au depasit limita de km

<ol>
@foreach($expenses as $expense)
     <li><b>{{ $expense->name }}</b> pentru <b>{{ $expense->plate }}</b> acum <b>{{ $expense->km - $expense->expiry_odometer }} km</b></li>
@endforeach
</ol>

{{ config('app.name') }}
@endcomponent
