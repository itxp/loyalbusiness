@component('mail::message')
# Au expirat

<ol>
@foreach($expenses as $expense)
        <li><b>{{ $expense->name }}</b> pentru <b>{{ $expense->car->plate }}</b> la <b>{{ $expense->expiry_date }}</b></li>
@endforeach
</ol>

{{ config('app.name') }}
@endcomponent
