<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Aloha!</title>

    <style type="text/css">
        * {
            font-family: Verdana, Arial, sans-serif;
        }
        table{
            font-size: x-small;
        }
        tfoot tr td{
            font-weight: bold;
            font-size: x-small;
        }
        .gray {
            background-color: #eee
        }
    </style>

</head>
<body>

<table width="100%">
    <tr>
        <td valign="top"><img src="{{ asset('img/logo.png') }}"/></td>
        <td align="right">
            <h3>Loyal Business Management SRL</h3>
            <pre>
                CIF: RO28876168 ORC: J03/1017/2011
                Adresa: Str. Depozitelor, Nr. 24, Romania, Pitesti, Arges
            </pre>
            <pre>
                BANCA: ING BANK N.V. SWIFT: INGBROBU
                IBAN(RON): RO19 INGB 0000 9999 0692 3556
                IBAN(EUR): RO62 INGB 0000 9999 0692 3558
            </pre>
            <pre>
                Email:office@loialbusiness.ro
                Telefon:0735445299
                www.loialbusiness.ro
            </pre>
        </td>
    </tr>
</table>

<h2 align="center">Comanda #001 / 16.01.2018</h2>

<table width="100%">
    <tr>
        <td style="background-color: #eee;">Catre</td>
        <td colspan="3">DORALEX TOP SPEDITION SRL CIF: RO36477796 ORC: J03/1476/2016</td>
    </tr>
    <tr>
        <td style="background-color: #eee;">Adresa</td>
        <td colspan="3">Romania, Arges, Pitesti, Calea Campulung 34</td>
    </tr>
    <tr>
        <td style="background-color: #eee; width: 100px">Tel</td>
        <td>0756586995</td>
        <td style="background-color: #eee; width: 100px">Email</td>
        <td>doralextop@gmail.com</td>
    </tr>
    <tr>

        <td style="background-color: #eee;">NR. auto</td>
        <td>AG44DJR</td>
        <td style="background-color: #eee;">Sofer</td>
        <td>Raul Radulescu</td>
    </tr>
    <tr>
        <td style="background-color: #eee;">Regim transport</td>
        <td>Exclusiv</td>
        <td style="background-color: #eee;">Pret</td>
        <td>500 EUR</td>
    </tr>
</table>
<hr>
<table width="100%">
    <tr>
        <th scope="row">Marfa #1</th>
        <td colspan="3">BANCA: ING BANK N.V. SWIFT: INGBROBU
            IBAN(RON): RO19 IN</td>
    </tr>
    <tr>
        <th scope="row">Referinta</th>
        <td colspan="3">xxx</td>
    </tr>
    <tr>
        <th scope="row">Descriere</th>
        <td colspan="3">sa vi cu chilotii in vine adriene</td>
    </tr>

    <tr style="background-color: #eee;">
        <th colspan="2" align="center">Incarcare</th>
        <th colspan="2" align="center">Descarcare</th>
    </tr>
    <tr>
        <th scope="row" style="background-color: #eee;" width="100px">Companie</th>
        <td>BMW</td>
        <th scope="row" style="background-color: #eee;" width="100px">Companie</th>
        <td>Reno<</td>
    </tr>
    <tr>
        <th scope="row" style="background-color: #eee;">Tara</th>
        <td>Romania</td>
        <th scope="row" style="background-color: #eee;">Tara</th>
        <td>Franta<</td>
    </tr>
    <tr>
        <th scope="row" style="background-color: #eee;">Data</th>
        <td>2018/03/21 - dupa ore xx</td>
        <th scope="row" style="background-color: #eee;">Data</th>
        <td>2018/03/21<</td>
    </tr>

    <tr>
        <th scope="row" style="background-color: #eee;">Adresa</th>
        <td>Str. xxx pe masa</td>
        <th scope="row" style="background-color: #eee;">Adresa</th>
        <td>adrian eremia nr 88adrian eremia nr 8</td>
    </tr>
    <tr><td colspan="4"><hr></td></tr>
    <tr>
        <th scope="row">Marfa #2</th>
        <td colspan="3">BANCA: ING BANK </td>
    </tr>
    <tr>
        <th scope="row">Referinta</th>
        <td colspan="3">xxx</td>
    </tr>
    <tr>
        <th scope="row">Descriere</th>
        <td colspan="3">sa vi cu chilotii in vine adriene</td>
    </tr>

    <tr style="background-color: #eee;">
        <th colspan="2" align="center">Incarcare</th>
        <th colspan="2" align="center">Descarcare</th>
    </tr>
    <tr>
        <th scope="row" style="background-color: #eee;">Companie</th>
        <td>BMW</td>
        <th scope="row" style="background-color: #eee;">Companie</th>
        <td>Reno<</td>
    </tr>
    <tr>
        <th scope="row" style="background-color: #eee;">Tara</th>
        <td>Romania</td>
        <th scope="row" style="background-color: #eee;">Tara</th>
        <td>Franta<</td>
    </tr>
    <tr>
        <th scope="row" style="background-color: #eee;">Data</th>
        <td>2018/03/21 - dupa ore xx</td>
        <th scope="row" style="background-color: #eee;">Data</th>
        <td>2018/03/21<</td>
    </tr>

    <tr>
        <th scope="row" style="background-color: #eee;">Adresa</th>
        <td>Str. xxx pe masa</td>
        <th scope="row" style="background-color: #eee;">Adresa</th>
        <td>adrian eremia nr 88<</td>
    </tr>
    <tr><td colspan="4"><hr></td></tr>

</table>

<h5>Termeni si conditii</h5>
<p style="font-size: 8px">
MODALITATE DE PLATA: PLATA PRIN TRANSFER BANCAR(ORDIN DE PLATA)LA 45 DE ZILE DE LA DATA PRIMIRII FACTURII SI A CMR-ULUI CONFIRMAT FARA REZERVE, IN ORIGINAL.<br>
Camionul poate pleca de la locul de incarcare numai dupa ce vom fi informati asupra volumului si greutatii marfii pe care a incarcat-o!<br>
Transportatorul are obligatia sa ia in primire marfurile pe baza avizelor, facturilor de insotire, iar daca are rezerve privind starea marfurilor sa le mentioneze pe CMR in momentul incarcarii. In caz contrar se va considera ca marfurile nu prezentau vicii in momentul incarcarii.<br>
Transportatorul este direct raspunzator de integritatea marfii pe parcursul derularii transportului.<br>
Daca autoutilitara nu se prezinta la incarcat sau nu respecta termenele de incarcare si descarcare, in urma confirmarii prezentei comenzi, se percepe o penalizare de 400 Euro pe zi.<br>
Aceasta comanda are valoare de contract. Confirmarea prezentei comenzi (prin stampila si semnatura) nu este necesara in cazul transmiterii electronice prin modem.<br>
Prin contactarea directa a clientului se penalizeaza cu pana la 40 000 Euro.<br>
VA ROG SA NE INSTIINTATI TELEFONIC LA NUMARUL DE TELEFON DE MAI JOS DUPA INCARCARE SI DESCARCARE! 0737445299 sau 0735445299<br>
Camionul poate pleca de la locul de incarcare numai dupa ce vom fi informati asupra volumului si greutatii marfii pe care a incarcat-o acesta!<br>
VA RUGAM SA NE TRIMITETI 2 CMR-uri IN ORIGINAL!
</p>
<table width="100%">
    <tr>
        <td><h3>Loyal Business Management SRL</h3><br>
            Fassbender Nicoleta
            <img src="{{ asset('img/stamp.png') }}"/>
        </td>
        <td align="right">
            <h3>Loyal Business Management SRL</h3><br>
            Fassbender Nicoleta
        </td>
    </tr>
</table>
</body>
</html>