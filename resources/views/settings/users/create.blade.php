@extends('layouts.main')

@section('head-inject')
  <!-- DataTables -->
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Useri
      <small>Adauga user</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">General Elements</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">User nou</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="POST" action="{{ route('settings.users.store') }}">
            {{ csrf_field() }}
            <div class="box-body">
              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif
              <div class="form-group">
                <label for="inputName">Nume</label>
                <input type="text" class="form-control" id="inputName" placeholder="Vasilica George" name="name" value="{{ old('name') }}" required>
              </div>
              <div class="form-group">
                <label for="inputEmail">Email</label>
                <input type="email" class="form-control" id="inputEmail" placeholder="exemplu@loialbusiness.ro" name="email" value="{{ old('email') }}" required>
              </div>
              <div class="form-group">
                <label for="inputPassword">Parola</label>
                <input type="text" class="form-control" id="inputPassword" placeholder="qwerty123" name="password" value="{{ old('password') }}" required>
              </div>
              <div class="form-group">
                <label for="inputSsn">CI / CNP</label>
                <input type="text" class="form-control" id="inputSsn" placeholder="AS 827665 / 1850302675432" name="ssn" value="{{ old('ssn') }}" required>
              </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Salveaza</button>
            </div>
          </form>
        </div>
      </div>
        <!-- /.box -->

    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

@endsection

@section('footer-inject')

@endsection