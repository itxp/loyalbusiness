@extends('layouts.main')

@section('head-inject')
  <!-- DataTables -->
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Flota
      <small>Modifica cheltuiala</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">General Elements</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Modifica cheltuiala</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="POST" action="{{ route('cars.expenses.update', [$car->id, $expense->id]) }}">

            {{ csrf_field() }}

            <input type="hidden" name="_method" value="PUT">

            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">

            <div class="box-body">
              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif
              <div class="form-group">
                <label for="inputPlate">Numar auto</label>
                <input type="text" class="form-control" id="inputPlate" value="{{ $car->plate }}" name="plate" disabled>
              </div>
              <div class="form-group">
                <label for="inputName">Nume</label>
                <input type="text" class="form-control" id="inputName" placeholder="Ex. Revizie sau placute fata" value="{{ $expense->name }}" name="name">
              </div>
              <div class="form-group">
                <label for="inputOdemeter">*Km</label>
                <input type="number" class="form-control" id="inputOdemeter" placeholder="299000" value="{{ $expense->odometer }}" name="odometer">
              </div>
              <div class="form-group">
                <label for="date">*Data</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="date" name="date" value="{{ $expense->date }}">
                </div>
                <!-- /.input group -->
              </div>
              <div class="form-group">
                <label for="inputCost">*Cost</label>
                <input type="number" class="form-control" id="inputCost" placeholder="0.00"  value="{{ $expense->cost }}" name="cost">
              </div>
              <div class="form-group">
                <label for="currency">*Moneda</label>
                <select class="form-control" id="currency" name="currency" required>
                  @if(old('currency'))
                    <option value="EUR"@if(old('currency')=='EUR') selected @endif>EUR</option>
                    <option value="RON"@if(old('currency')=='RON') selected @endif>RON</option>
                  @else
                    <option value="EUR"@if($expense->currency=='EUR') selected @endif>EUR</option>
                    <option value="RON"@if($expense->currency=='RON') selected @endif>RON</option>
                  @endif
                </select>
              </div>
              <div class="form-group">
                <label for="inputExpireOdemeter">Expira la km</label>
                <input type="number" class="form-control" id="inputExpireOdemeter" placeholder="199000" value="{{ $expense->expiry_odometer }}" name="expiry_odometer">
              </div>
              <div class="form-group">
                <label for="expiryDate">Expira la data</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="expiryDate" name="expiry_date" value="{{ $expense->expiry_date }}">
                </div>
                <!-- /.input group -->
              </div>
              <div class="form-group">
                <label for="inputDescription">Descriere</label>
                <textarea id="inputDescription" class="form-control" rows="3" placeholder="Descriere ..." name="description">{{ $expense->description }}</textarea>
              </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Modifica</button>
            </div>
          </form>
        </div>
      </div>
        <!-- /.box -->

    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

@endsection

@push('css')

  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

@endpush

@push('scripts')

  <!-- bootstrap datepicker -->
  <script src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script>
      //Date picker
      $('#expiryDate').datepicker({
          format: 'dd/mm/yyyy',
          autoclose: true
      });
      //Date picker
      $('#date').datepicker({
          format: 'dd/mm/yyyy',
          autoclose: true
      })
  </script>

@endpush