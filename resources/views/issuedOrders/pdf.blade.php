<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Comanda #{{ $issuedOrder->id }}</title>

    <style type="text/css">
        * {
            font-family: Verdana, Arial, sans-serif;
        }
        table{
            font-size: x-small;
        }
        tfoot tr td{
            font-weight: bold;
            font-size: x-small;
        }
        .gray {
            background-color: #eee
        }
    </style>

</head>
<body>

<table width="100%">
    <tr>
        <td valign="top"><img src="{{ asset('img/logo.png') }}"/></td>
        <td align="right">
            {!! $header !!}
            {{--<h3>Loyal Business Management SRL</h3>--}}
            {{--<pre>--}}
                {{--CIF: RO28876168 ORC: J03/1017/2011--}}
                {{--Adresa: Str. Depozitelor, Nr. 24, Romania, Pitesti, Arges--}}
            {{--</pre>--}}
            {{--<pre>--}}
                {{--BANCA: ING BANK N.V. SWIFT: INGBROBU--}}
                {{--IBAN(RON): RO19 INGB 0000 9999 0692 3556--}}
                {{--IBAN(EUR): RO62 INGB 0000 9999 0692 3558--}}
            {{--</pre>--}}
            {{--<pre>--}}
                {{--Email:office@loialbusiness.ro--}}
                {{--Telefon:0735445299--}}
                {{--www.loialbusiness.ro--}}
            {{--</pre>--}}
        </td>
    </tr>
</table>

<h2 align="center">Comanda #{{ $issuedOrder->id }} - {{ $issuedOrder->date }}</h2>

<table width="100%">
    <tr>
        <td style="background-color: #eee;">Catre</td>
        <td colspan="3">{{ $issuedOrder->carrier->name }} CIF: {{ $issuedOrder->carrier->cif }} ORC: {{ $issuedOrder->carrier->orc }}</td>
    </tr>
    <tr>
        <td style="background-color: #eee;">Adresa</td>
        <td colspan="3">{{ $issuedOrder->carrier->address }} {{ $issuedOrder->carrier->country->name }}</td>
    </tr>
    <tr>
        <td style="background-color: #eee; width: 100px">Tel</td>
        <td>{{ $issuedOrder->carrier->phone }}</td>
        <td style="background-color: #eee; width: 100px">Email</td>
        <td>{{ $issuedOrder->carrier->email }}</td>
    </tr>
    <tr>

        <td style="background-color: #eee;">NR. auto</td>
        <td>{{ $issuedOrder->plate }}</td>
        <td style="background-color: #eee;">Sofer</td>
        <td>{{ $issuedOrder->driver }}</td>
    </tr>
    <tr>
        <td style="background-color: #eee;">Regim transport</td>
        <td>{{ $issuedOrder->type }}</td>
        <td style="background-color: #eee;">Pret</td>
        <td>{{ $issuedOrder->price }} {{ $issuedOrder->currency }}</td>
    </tr>
</table>
<hr>
<table width="100%">
    @php($i=1)
    @foreach($issuedOrder->freights as $freight)
        <tr>
            <th scope="row">Marfa #{{ $i++ }}</th>
            <td colspan="3">{{ $freight->freight }}</td>
        </tr>
        <tr>
            <th scope="row">Referinta</th>
            <td colspan="3">{{ $freight->reference }}</td>
        </tr>
        <tr>
            <th scope="row">Descriere</th>
            <td colspan="3">{{ $freight->description }}</td>
        </tr>

        <tr style="background-color: #eee;">
            <th colspan="2" align="center">Incarcare</th>
            <th colspan="2" align="center">Descarcare</th>
        </tr>
        <tr>
            <th scope="row" style="background-color: #eee;" width="100px">Companie</th>
            <td>{{ $freight->load_company }}</td>
            <th scope="row" style="background-color: #eee;" width="100px">Companie</th>
            <td>{{ $freight->unload_company }}<</td>
        </tr>
        <tr>
            <th scope="row" style="background-color: #eee;">Tara</th>
            <td>{{ $freight->loadCountry->name }}</td>
            <th scope="row" style="background-color: #eee;">Tara</th>
            <td>{{ $freight->unloadCountry->name }}<</td>
        </tr>
        <tr>
            <th scope="row" style="background-color: #eee;">Data</th>
            <td>{{ $freight->load_date }}</td>
            <th scope="row" style="background-color: #eee;">Data</th>
            <td>{{ $freight->unload_date }}</td>
        </tr>
        <tr>
            <th scope="row" style="background-color: #eee;">Mentiuni</th>
            <td>{{ $freight->load_mentions }}</td>
            <th scope="row" style="background-color: #eee;">Mentiuni</th>
            <td>{{ $freight->unload_mentions }}</td>
        </tr>
        <tr>
            <th scope="row" style="background-color: #eee;">Adresa</th>
            <td>{{ $freight->load_address }}</td>
            <th scope="row" style="background-color: #eee;">Adresa</th>
            <td>{{ $freight->unload_address }}</td>
        </tr>
        <tr><td colspan="4"><hr></td></tr>
    @endforeach

</table>

<h5>Termeni si conditii</h5>
<p style="font-size: 8px">
    {{ $issuedOrder->tos }}
</p>
<table width="100%">
    <tr>
        <td><h3>Loyal Business Management SRL</h3><br>
            {{ $issuedOrder->user->name }}<br>
            <img src="{{ asset('img/stamp.png') }}" style="width: 1in"/>
        </td>
        <td align="right">
            <h3>{{ $issuedOrder->carrier->name }}</h3><br>
            {{ $issuedOrder->carrier->contact }}
        </td>
    </tr>
</table>
</body>
</html>