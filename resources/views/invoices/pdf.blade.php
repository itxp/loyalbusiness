<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Factura-{{ $invoice->series }}#{{ $invoice->number }}</title>

    <style type="text/css">
        * {
            font-family: Verdana, Arial, sans-serif;
        }
        table{
            font-size: x-small;
        }
        tfoot tr td{
            font-weight: bold;
            font-size: x-small;
        }
        .gray {
            background-color: #eee
        }
    </style>

</head>
<body>

<table width="100%">
    <tr>
        <td valign="top"><img src="{{ asset('img/logo.png') }}"/></td>
        <td align="right">
            {!! $header !!}
            {{--<h3>Loyal Business Management SRL</h3>--}}
            {{--<pre>--}}
                {{--CIF: RO28876168 ORC: J03/1017/2011--}}
                {{--Adresa: Str. Depozitelor, Nr. 24, Romania, Pitesti, Arges--}}
            {{--</pre>--}}
            {{--<pre>--}}
                {{--BANCA: ING BANK N.V. SWIFT: INGBROBU--}}
                {{--IBAN(RON): RO19 INGB 0000 9999 0692 3556--}}
                {{--IBAN(EUR): RO62 INGB 0000 9999 0692 3558--}}
            {{--</pre>--}}
            {{--<pre>--}}
                {{--Email:office@loialbusiness.ro--}}
                {{--Telefon:0735445299--}}
                {{--www.loialbusiness.ro--}}
            {{--</pre>--}}
        </td>
    </tr>
</table>
<div align="center">
    <h2>Factura {{ $invoice->series }} {{ $invoice->number }} - {{ $invoice->date }}</h2>
    Scadenta la data de {{ $invoice->due_date }}
</div>

<table width="100%">
    <tr>
        <td style="background-color: #eee;">Cumparator</td>
        <td colspan="3">{{ $invoice->buyer['name'] }}</td>
    </tr>
    <tr>
        <td style="background-color: #eee; width: 100px">CIF</td>
        <td>{{ $invoice->buyer['cif'] }}</td>
        <td style="background-color: #eee; width: 100px">ORC</td>
        <td>{{ $invoice->buyer['orc'] }}</td>
    </tr>
    <tr>
        <td style="background-color: #eee;">Adresa</td>
        <td colspan="3">{{ $invoice->buyer['country'] }} {{ $invoice->buyer['address'] }}</td>
    </tr>
    <tr>
        <td style="background-color: #eee; width: 100px">Banca</td>
        <td>{{ $invoice->buyer['bank'] }}</td>
        <td style="background-color: #eee; width: 100px">Cont</td>
        <td>{{ $invoice->buyer['iban'] }}</td>
    </tr>
    <tr>
        <td style="background-color: #eee;">Telefon</td>
        <td>{{ $invoice->buyer['phone'] }}</td>
        <td style="background-color: #eee;">Email</td>
        <td>{{ $invoice->buyer['email'] }}</td>
    </tr>
</table>
<hr>
<table width="100%">
        <tbody>
        <tr valign="top">
            <th style="width: 10px" align="center">#</th>
            <th>Descriere</th>
            <th width="30px" align="center" style="">UM</th>
            <th width="55px" align="center">Cantitate</th>
            <th width="100px" align="center">Pret unitar<br>(Fara TVA)</th>
            <th width="100px" align="center">Valoare</th>
            <th width="100px" align="center">Valoare<br>TVA</th>
        </tr>
        @php($i=1)
        @php($price_ron=0)
        @php($price_eur=0)
        @if(!is_null($invoice->details))
            @foreach($invoice->details as $key =>$detail)
                <tr valign="top">
                    <td style="width: 10px">{{ $i++ }}</td>
                    <td>{!! $detail['description'] !!}</td>
                    <td width="30px" align="center">{{ $detail['unit'] }}</td>
                    <td width="55px" align="center">{{ $detail['quantity'] }}</td>
                    <td width="100px" align="center">{{ number_format($detail['price_ron'],2,',','.') }} RON<br>{{ number_format($detail['price_eur'],2,',','.') }} EUR</td>
                    <td width="100px" align="center">{{ number_format($detail['quantity']*$detail['price_ron'],2,',','.') }} RON<br>{{ number_format($detail['quantity']*$detail['price_eur'],2,',','.') }} EUR</td>
                    <td width="100px" align="center">{{ number_format($detail['quantity']*$detail['price_ron']*$invoice->vat/100,2,',','.') }} RON<br>{{ number_format($detail['quantity']*$detail['price_eur']*$invoice->vat/100,2,',','.') }} EUR</td>
                    @php($price_ron+=$detail['price_ron']*$detail['quantity'])
                    @php($price_eur+=$detail['price_eur']*$detail['quantity'])

                </tr>
            @endforeach
        @endif
        @if($invoice->discount)
        <tr valign="top">
            <td style="width: 10px">{{ $i++ }}</td>
            <td>Discount {{ $invoice->discount }}% cu plata la primirea documentelor</td>
            <td width="30px" align="center">%</td>
            <td width="55px" align="center">{{ $invoice->discount }}</td>
            <td width="100px" align="center">-{{ number_format($discount_ron = $price_ron*$invoice->discount/100,2,',','.') }} RON<br>-{{ number_format($discount_eur = $price_eur*$invoice->discount/100,2,',','.') }} EUR</td>
            <td width="100px" align="center">-{{ number_format($discount_ron,2,',','.') }} RON<br>-{{ number_format($discount_eur,2,',','.') }} EUR</td>
            <td width="100px" align="center">-{{ number_format($discount_ron*$invoice->vat/100,2,',','.') }} RON<br>-{{ number_format($discount_eur*$invoice->vat/100,2,',','.') }} EUR</td>
        </tr>
        @endif
        </tbody>

</table>
<hr>
<table width="100%">
    <tbody>
    <tr>
        <td style="background-color: #eee; width: 100px">Cota TVA</td>
        <td>{{ $invoice->vat }} %</td>
        <td align="center" width="100px" rowspan="2" style="background-color: #eee; width: 100px"><b>Total</b></td>
        <td align="center" width="100px"><b>{{ number_format($price_ron-$discount_ron,2,',','.') }} RON</b></td>
        <td align="center" width="100px"><b>{{ number_format(($price_ron-$discount_eur)*$invoice->vat/100,2,',','.') }} RON</b></td>
    </tr>
    <tr>
        <td style="background-color: #eee; width: 100px">Curs valutar</td>
        <td>1 EUR = {{ $invoice->exchange_rate }} RON</td>
        <td align="center" width="100px"><b>{{ number_format($price_eur-$discount_eur,2,',','.') }} EUR</b></td>
        <td align="center" width="100px"><b>{{ number_format(($price_eur-$discount_eur)*$invoice->vat/100,2,',','.') }} EUR</b></td>
    </tr>
    <tr>
        <td style="background-color: #eee; width: 100px">Operator</td>
        <td>{{ $invoice->user->name }}</td>
        <td align="center" width="100px" rowspan="2" style="background-color: #eee; width: 100px"><b>Total de plata</b></td>
        <td align="center" colspan="2"><b>{{ number_format($invoice->total_ron,2,',','.') }} RON</b></td>
    </tr>
    <tr>
        <td style="background-color: #eee; width: 100px">CI / CNP</td>
        <td>{{ $invoice->user->ssn }}</td>
        <td align="center" colspan="2" width="200px"><b>{{ number_format($invoice->total_eur,2,',','.') }} EUR</b></td>
    </tr>
    </tbody>
</table>
<hr>
<table width="100%">
    <tbody>
    <tr>
        <td align="center" valign="top" width="150px">Semnatura / Stampila Furnizor<br><img src="{{ asset('img/stamp.png') }}" style="width: 1in"/></td>
        <td valign="middle" style="border-left: 1px solid; border-right: 1px solid">Numele delegatului: {{ $invoice->delegate['name'] }}<br>
            Identificat cu BI/CI/PS Seria: {{ $invoice->delegate['id_series'] }} Nr: {{ $invoice->delegate['id_number'] }}<br>
            Eliberat de: {{ $invoice->delegate['id_issuer'] }}<br>
            Mijloc de transport: {{ $invoice->delegate['id_issuer'] }}<br>
            Expeditia s-a facut in prezenta noatra la data de: {{ $invoice->delegate['date'] }} ora: {{ $invoice->delegate['time'] }}<br>
            Semnaturi:
        </td>
        <td align="center" valign="top" width="150px">Semnatura / Stampila Cumparator</td>

    </tr>

    </tbody>
</table>

{{--<h5>Termeni si conditii</h5>--}}
{{--<p style="font-size: 8px">--}}
    {{--{{ $issuedOrder->tos }}--}}
{{--</p>--}}
{{--<table width="100%">--}}
    {{--<tr>--}}
        {{--<td><h3>Loyal Business Management SRL</h3><br>--}}
            {{--{{ $issuedOrder->user->name }}<br>--}}
            {{--<img src="{{ asset('img/stamp.png') }}" style="width: 1in"/>--}}
        {{--</td>--}}
        {{--<td align="right">--}}
            {{--<h3>{{ $issuedOrder->carrier->name }}</h3><br>--}}
            {{--{{ $issuedOrder->carrier->contact }}--}}
        {{--</td>--}}
    {{--</tr>--}}
{{--</table>--}}
</body>
</html>