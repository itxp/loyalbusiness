@extends('layouts.main')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Comenzi primite
      <small>Adauga comanda primita</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">General Elements</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Comanda primita</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="POST" action="{{ route('invoices.details.store', [$invoice->id]) }}">
            {{ csrf_field() }}
            <input type="hidden" id="received_order_id" name="received_order_id">
{{--            <input type="hidden" id="client_id" name="user_id" value="{{ Auth::user()->id }}">--}}
            <div class="box-body">
              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif
              <div class="form-group">
                  <label for="inputDescription">*Descriere</label>
                  <textarea id="inputDescription" class="form-control" rows="3" placeholder="Descriere ..." name="description" required></textarea>
              </div>
              <div class="form-group">
                  <label for="inputUnit">Unitate masura</label>
                  <input type="text" class="form-control" id="inputUnit" placeholder="Tr / buc" value="{{ old('unit') }}" name="unit" required>
              </div>
              <div class="form-group">
                <label for="inputQuantity">*Cantitate</label>
                <input type="number" class="form-control" id="inputQuantity" placeholder="1" name="quantity" value="{{ old('quantity')?old('quantity'):1 }}" required>
              </div>
              <div class="form-group">
                <label for="inputPriceEur">*Pret EUR(fara tva)</label>
                <input type="number" step="0.01" class="form-control" id="inputPriceEur" placeholder="435" name="price_eur" value="{{ old('price_eur') }}" required>
              </div>
              <div class="form-group">
                <label for="inputPriceRon">*Pret RON(fara tva)</label>
                <input type="number" step="0.01" class="form-control" id="inputPriceRon" placeholder="135" name="price_ron" value="{{ old('price_ron') }}" required>
              </div>

            </div>

            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Salveaza</button>
            </div>
          </form>
        </div>
          <div class="box box-primary">
          <div class="box-body">
              <div class="box-header with-border">
                  <h3 class="box-title">Comenzi primite nefacturate {{ $invoice->client->name }}</h3>
              </div>


              <table class="table table-bordered" id="details-table" width="100%">
                  <thead>
                  <tr>
                      <th>Numar</th>
                      <th>Data</th>
                      <th>Pret</th>
                      <th>Moneda</th>
                      <th>Actiuni</th>
                  </tr>
                  </thead>
              </table>

          </div>
          </div>
      </div>
        <!-- /.box -->
        <!-- /.box-body -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

@endsection



@push('scripts')
  <!-- bootstrap datepicker -->
  {{--<script src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>--}}
  {{--<!-- bootstrap time picker -->--}}
  {{--<script src="/plugins/timepicker/bootstrap-timepicker.min.js"></script>--}}

  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
  <script src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#details-table').DataTable({
                processing: true,
                serverSide: true,
                rowId: 'id',
                dom: 'Bfrtip',
                ajax: '{!! route('data.receivedOrdersNoInvoice', [$invoice->id]) !!}',
                columns: [
                    { data: 'number', name: 'number' },
                    { data: 'date', name: 'date' },
                    { data: 'price', name: 'price' },
                    { data: 'currency', name: 'currency' },
                    { data: 'action', name: 'action' },

                ],
                // 'autoWidth'   : true,

                select:  {
                    style: 'multi'
                },

                "scrollX": true,
                "order": [[ 0, "desc" ]],

                buttons: [
                    {
                        text: 'Selecteaza tot',
                        action: function () {
                            this.rows().select();
                        }
                    },
                    {
                        text: 'Deselecteaza tot',
                        action: function () {
                            this.rows().deselect();
                        }
                    },
                    {
                        text: 'Adauga pe factura',
                        action: function ( e, dt, items, indexes ) {
                            ids = dt.rows( { selected: true } ).ids().toArray();
                            $.ajax({
                                type: 'POST',
                                url: '{!! route('data.attachDetails', [$invoice->id]) !!}',
                                data: { ids: ids },
                                error: function() {
                                    alert('A aparut o eroare!')
                                }
                            });
                            dt.ajax.reload();
                        }
                    }
                ]
            });
        });

        rate = {{ $invoice->exchange_rate }};

        $("#inputPriceEur").on('change', function() {

            $("#inputPriceRon").val(($("#inputPriceEur").val() * rate).toFixed(2));

        });
        $("#inputPriceRon").on('change', function() {

            $("#inputPriceEur").val(($("#inputPriceRon").val() / rate).toFixed(2));

        });

        {{--$(function() {--}}
          {{--$.ajaxSetup({--}}
              {{--headers: {--}}
                  {{--'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
              {{--}--}}
          {{--});--}}

          {{--$('#details-table').DataTable({--}}
              {{--processing: true,--}}
              {{--serverSide: true,--}}
              {{--rowId: 'id',--}}
              {{--dom: 'Bfrtip',--}}
              {{--ajax: '{!! route('data.receivedOrdersNoInvoice', [$invoice->id]) !!}',--}}
              {{--columns: [--}}
                  {{--{ data: 'number', name: 'number' },--}}
                  {{--{ data: 'date', name: 'date' },--}}
                  {{--{ data: 'price', name: 'price' },--}}
                  {{--{ data: 'currency', name: 'currency' },--}}
                  {{--{ data: 'action', name: 'action' },--}}

              {{--],--}}
              {{--// 'autoWidth'   : true,--}}

              {{--select:  {--}}
                  {{--style: 'multi'--}}
              {{--},--}}

              {{--"scrollX": true,--}}
              {{--"order": [[ 0, "desc" ]],--}}

              {{--buttons: [--}}
                  {{--{--}}
                      {{--text: 'Selecteaza tot',--}}
                      {{--action: function () {--}}
                          {{--this.rows().select();--}}
                      {{--}--}}
                  {{--},--}}
                  {{--{--}}
                      {{--text: 'Deselecteaza tot',--}}
                      {{--action: function () {--}}
                          {{--this.rows().deselect();--}}
                      {{--}--}}
                  {{--},--}}
                  {{--{--}}
                      {{--text: 'Adauga la decont',--}}
                      {{--action: function ( e, dt, items, indexes ) {--}}
                          {{--details = dt.rows( { selected: true } ).ids().toArray();--}}
                          {{--$.ajax({--}}
                              {{--type: 'POST',--}}
                              {{--url: '{!! route('data.attachDetails', [$invoice->id]) !!}',--}}
                              {{--data: { details: details },--}}
                              {{--error: function() {--}}
                                  {{--alert('A aparut o eroare!')--}}
                              {{--}--}}
                          {{--});--}}
                          {{--dt.ajax.reload();--}}
                      {{--}--}}
                  {{--}--}}
              {{--]--}}
          {{--});--}}
      // });
  </script>
@endpush