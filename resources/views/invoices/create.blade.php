@extends('layouts.main')

@section('head-inject')
  <!-- DataTables -->
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Comenzi primite
      <small>Adauga comanda primita</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">General Elements</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Comanda primita</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="POST" action="{{ route('invoices.store') }}">
            {{ csrf_field() }}
            <input type="hidden" id="client_id" name="client_id">
            <input type="hidden" id="client_id" name="user_id" value="{{ Auth::user()->id }}">
            <div class="box-body">
              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif
              <div class="form-group">
                <div class="dropdown">
                  <button id="client-button" class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">*Selecteaza client
                    <span class="caret"></span></button>
                  <ul class="dropdown-menu" id="client-select">
                    <input class="form-control" id="myInput" type="text" placeholder="Cauta...">

                    @foreach($clients as $client)
                      <li id="{{ $client->id }}"><a href="#">{{ $client->name }}</a></li>
                    @endforeach

                  </ul>
                </div>
              </div>
              <div class="form-group">
                  <label for="series">*Seria</label>
                  <select class="form-control" id="series" name="series" required>
                      <option value>Alege seria</option>
                      <option value="LBM"@if(old('series')=='LBM') selected @endif>LBM</option>
                      <option value="LOY"@if(old('series')=='LOY') selected @endif>LOY</option>
                  </select>
                  </div>
              <div class="form-group">
                <label for="inputNumber">*Numar</label>
                <input type="text" class="form-control" id="inputNumber" placeholder="435" name="number" value="{{ old('number') }}" required>
              </div>
              <div class="form-group">
                <label for="date">*Data</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="date" name="date" value="{{ old('date')?old('date'):now()->format('d/m/Y') }}" required>
                </div>
                <!-- /.input group -->
              </div>
              <div class="form-group">
                  <label for="due_date">*Scadenta</label>
                  <div class="input-group date">
                      <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="due_date" name="due_date" value="{{ old('date')?old('date'):now()->format('d/m/Y') }}" required>
                  </div>
                  <!-- /.input group -->
              </div>
              <div class="form-group">
                  <label for="inputExchangeRate">*Cur valutar 1 EUR =</label>
                  <input type="number" step="any" class="form-control" id="inputExchangeRate" placeholder="0"  value="{{ old('exchange_rate')?old('exchange_rate'):$exchangeRate->rate }}" name="exchange_rate" required>
              </div>
              <div class="form-group">
                  <label for="inputDiscount">*Discount %</label>
                  <input type="number" step="any" class="form-control" id="inputDiscount" placeholder="0"  value="{{ old('discount')?old('discount'):0 }}" name="discount" required>
              </div>
              <div class="form-group">
                  <label for="inputVat">*TVA %</label>
                  <input type="number" step="any" class="form-control" id="inputVat" placeholder="0"  value="{{ old('vat')?old('vat'):0 }}" name="vat" required>
              </div>
              <div class="form-group">
                  <label for="inputDelegateName">Numele delegatului</label>
                  <input type="text" class="form-control" id="inputDelegateName" placeholder="Posta sau Bobitza" value="{{ old('delegate.name') }}" name="delegate[name]" required>
              </div>
              <div class="form-group">
                  <label for="inputDelegateIdSeries">Serie buletin delegat</label>
                  <input type="text" class="form-control" id="inputDelegateIdSeries" placeholder="AS" value="{{ old('delegate.id_series') }}" name="delegate[id_series]">
              </div>
              <div class="form-group">
                  <label for="inputDelegateIdNumber">Numar buletin delegat</label>
                  <input type="text" class="form-control" id="inputDelegateIdNumber" placeholder="372133" value="{{ old('delegate.id_number') }}" name="delegate[id_number]">
              </div>
              <div class="form-group">
                  <label for="inputDelegateIdIssuer">Buletin emis de</label>
                  <input type="text" class="form-control" id="inputDelegateIdIssuer" placeholder="Pol. mun. Mioveni" value="{{ old('delegate.id_issuer') }}" name="delegate[id_issuer]">
              </div>
              <div class="form-group">
                  <label for="inputDelegateTransport">Mijloc transport</label>
                  <input type="text" class="form-control" id="inputDelegateTransport" placeholder="Taxi sau B81ERE" value="{{ old('delegate.transport') }}" name="delegate[transport]">
              </div>
              <div class="form-group">
                  <label for="inputDelegateDate">La data de</label>
                  <div class="input-group date">
                      <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="inputDelegateDate" name="delegate[date]" value="{{ old('delegate.date')?old('delegate.date'):now()->format('d/m/Y') }}">
                  </div>
                  <!-- /.input group -->
              </div>
              <div class="form-group">
                  <label for="inputDelegateTime">La Ora</label>
                  <input type="text" class="form-control" id="inputDelegateTime" placeholder="12:00" value="{{ old('delegate.time')?old('delegate.time'):date("H:i") }}" name="delegate[time]">
              </div>
            </div>

            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Salveaza</button>
            </div>
          </form>
        </div>
      </div>
        <!-- /.box -->

    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

@endsection

@push('css')

  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  {{--<!-- Bootstrap time Picker -->--}}
  {{--<link rel="stylesheet" href="/plugins/timepicker/bootstrap-timepicker.min.css">--}}


@endpush

@push('scripts')
  <!-- bootstrap datepicker -->
  <script src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  {{--<!-- bootstrap time picker -->--}}
  {{--<script src="/plugins/timepicker/bootstrap-timepicker.min.js"></script>--}}

  <script>
      //Date picker
      $('#date').datepicker({
          format: 'dd/mm/yyyy',
          autoclose: true
      });
      $('#due_date').datepicker({
          format: 'dd/mm/yyyy',
          autoclose: true
      });
      $('#inputDelegateDate').datepicker({
          format: 'dd/mm/yyyy',
          autoclose: true
      });
  </script>

  <script>
      $(document).ready(function(){
          $("#myInput").on("keyup", function() {
              // console.log('ok');
              var value = $(this).val().toLowerCase();
              $(".dropdown-menu li").filter(function() {
                  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
              });
          });
          $("#client-select li").click(function() {
              $("#client_id").val(this.id);
              $("#client-button").text($(this).find("a").text());
              $.getJSON( "{{ route('data.clients') }}/" + this.id, function( data ) {
                  $("#inputDiscount").val(data.discount);
                  $("#inputVat").val(data.vat);
              });
              // console.log($("#client-button").text("kkt"))
                  //= ($(this).find("a").text());
              // $("#client-button").html()
              // console.log($("#client-button").html());
          });
          $("#series").on('change', function() {
              $.get( "{{ route('data.invoice.max') }}/" + this.value, function( data ) {
                  $("#inputNumber").val(parseInt(data));
                  // console.log($("#inputNumber").val);
              });
          });
          $("#date").on('change', function() {
              $.getJSON( "{{ route('data.exchange') }}/" + this.value, function( data ) {
                  $("#inputExchangeRate").val(data.rate);
              });

          })

          // $("#series").on('change', function() {
          //     $("#number").val = numbers[this.value];
          // })
      });
  </script>
@endpush