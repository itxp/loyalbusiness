@extends('layouts.main')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">

@endpush

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Comenzi primite
      <small>Detalii comanda</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
          <div class="box box-primary">
              <div class="box-header with-border">
                  <h3 class="box-title">Factura: <b>{{ $invoice->series }} {{ $invoice->number }}</b></h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                  <i class="fa fa-calendar margin-r-5"></i>Data: <b>{{ $invoice->date }}</b>

                  <hr>

                  <i class="fa fa-address-card margin-r-5"></i> Cumparator: <b>{{ $invoice->buyer['name'] }}</b>

                  <hr>

                  <i class="fa fa-money margin-r-5"></i> Total RON: <b>{{ $invoice->total_ron }}</b>

                  <hr>

                  <i class="fa fa-money margin-r-5"></i> Total EUR: <b>{{ $invoice->total_eur }}</b>

                  <hr>

                  <a href="{{ route('invoices.details.create', [$invoice->id]) }}" class="btn btn-success">Adauga produs/serviciu</a>
                  <a href="{{ route('invoices.pdfOpen', [$invoice->id]) }}" class="btn btn-primary" target="_blank">Vizualizeaza</a>
                  <a href="{{ route('invoices.pdfDownload', [$invoice->id]) }}" class="btn btn-info">Descarca</a>

              </div>

          <!-- /.box-body -->
          </div>

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Produse si servicii</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered" id="details">
                    <tbody>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Descriere</th>
                        <th>UM</th>
                        <th>Cantitate</th>
                        <th>Pret unitar<br>(Fara TVA)</th>
                        <th>Valoare</th>
                        <th>Valoare<br>TVA</th>
                        <th>Sterge /<br>Elimina</th>
                    </tr>
                    @php($i=1)
                    @if(!is_null($invoice->details))
                        @foreach($invoice->details as $key =>$detail)
                            <tr id="{{ $key }}">
                                <td>{{ $i++ }}</td>
                                <td>{!! $detail['description'] !!}</td>
                                <td>{{ $detail['unit'] }}</td>
                                <td>{{ $detail['quantity'] }}</td>
                                <td>{{ number_format($detail['price_ron'],2,',','.') }} RON<br>{{ number_format($detail['price_eur'],2,',','.') }} EUR</td>
                                <td>{{ number_format($detail['quantity']*$detail['price_ron'],2,',','.') }} RON<br>{{ number_format($detail['quantity']*$detail['price_eur'],2,',','.') }} EUR</td>
                                <td>{{ number_format($detail['quantity']*$detail['price_ron']*$invoice->vat/100,2,',','.') }} RON<br>{{ number_format($detail['quantity']*$detail['price_eur']*$invoice->vat/100,2,',','.') }} EUR</td>
                                <td>
                                    <button type="button" class="close remove" aria-label="Close" onclick="callAjax({{ $key }})">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    @endif

                    </tbody></table>
            </div>

        </div>

      </div>
      {{--<!-- /.col -->--}}
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

@endsection

@push('scripts')
    <!-- bootstrap datepicker -->
    <script src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    {{--<!-- bootstrap time picker -->--}}
    {{--<script src="/plugins/timepicker/bootstrap-timepicker.min.js"></script>--}}

    <script>
        function callAjax(articleId) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('invoices.details.index', [$invoice->id]) }}/'+articleId,
                data: {_method: 'DELETE'},
                success:function(response) {
                    $('table#details tr#'+articleId).remove();
                },
                error: function() {
                        alert('A aparut o eroare!')
                }
            });
        }

    </script>
@endpush

