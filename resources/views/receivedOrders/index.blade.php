@extends('layouts.main')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endpush

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Comenzi primite
      <small>Lista comenzi primite</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Acasa</a></li>
        <li class="active">Comenzi primite</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header with-border">

                  <a href="{{ route('receivedOrders.create') }}" class="btn btn-success">Adauga comanda primita</a>


          </div>
          <!-- /.box-header -->
          <div class="box-body">

            <table class="table table-bordered" id="cars-table" width="100%">
              <thead>
              <tr>
                <th>Numar</th>
                <th>Client</th>
                <th>Data</th>
                <th>Pret</th>
                <th>Moneda</th>
                <th>Factura</th>
                <th>Actiuni</th>
              </tr>
              </thead>
            </table>

          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

@endsection

@push('scripts')

  <!-- DataTables -->
  <script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

  <!-- page script -->
  {{--<script>--}}
      {{--$(function () {--}}
          {{--// $('#example1').DataTable()--}}
          {{--$('#example1').DataTable({--}}
              {{--'paging'      : true,--}}
              {{--'lengthChange': false,--}}
              {{--// 'searching'   : false,--}}
              {{--// "columns": [--}}
              {{--//     { "searchable": false },--}}
              {{--//     null,--}}
              {{--//     null,--}}
              {{--//     null,--}}
              {{--//     null--}}
              {{--// ],--}}
              {{--'ordering'    : true,--}}
              {{--'info'        : true,--}}
              {{--'autoWidth'   : false,--}}
              {{--// 'filtering'   : true--}}
          {{--})--}}
      {{--})--}}
  {{--</script>--}}

  <script>
      $(function() {
          $('#cars-table').DataTable({
              processing: true,
              serverSide: true,
              ajax: '{!! route('data.receivedOrders') !!}',
              columns: [
                  { data: 'number', name: 'number' },
                  { data: 'name', name: 'clients.name' },
                  { data: 'date', name: 'date' },
                  { data: 'price', name: 'price' },
                  { data: 'currency', name: 'currency' },
                  { data: 'invoice', name: 'invoice' },
                  { data: 'action', name: 'action' },
              ],
              // 'autoWidth'   : true,

              "scrollX": true,
              "order": [[ 2, "desc" ]],
          });
      });
  </script>

@endpush