@extends('layouts.main')

@section('head-inject')
  <!-- DataTables -->
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Comenzi primite
      <small>Adauga comanda primita</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Acasa</a></li>
      <li><a href="{{ route('receivedOrders.index') }}">Comenzi primite</a></li>
      <li class="active">Adauga comanda primita</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Comanda primita</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="POST" action="{{ route('receivedOrders.store') }}">
            {{ csrf_field() }}
            <input type="hidden" id="client_id" name="client_id" @if(old('client_id'))value="{{ old('client_id') }}"@endif>
            <input type="hidden" id="client_name" name="client_name"@if(old('client_name'))value="{{ old('client_name') }}"@endif>
            <input type="hidden" id="user_id" name="user_id" value="{{ Auth::user()->id }}">
            <div class="box-body">
              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif
              <div class="form-group">
                <div class="dropdown">
                  <button id="client-button" class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">@if(old('client_name')) {{ old('client_name') }} @else *Selecteaza client @endif
                    <span class="caret"></span></button>
                  <ul class="dropdown-menu" id="client-select">
                    <input class="form-control" id="myInput" type="text" placeholder="Cauta...">

                    @foreach($clients as $client)
                      <li id="{{ $client->id }}"><a href="#">{{ $client->name }}</a></li>
                    @endforeach

                  </ul>
                </div>
              </div>
              <div class="form-group">
                <label for="inputNumber">*Numar comanda</label>
                <input type="text" class="form-control" id="inputNumber" placeholder="21563" name="number" value="{{ old('number') }}" required>
              </div>
              <div class="form-group">
                <label for="date">*Data</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="date" name="date" value="{{ old('date')?old('date'):now()->format('d/m/Y') }}" required>
                </div>
                <!-- /.input group -->
              </div>
              <div class="form-group">
                <label for="inputOpt">OPT / Extra</label>
                <input type="text" class="form-control" id="inputOpt" placeholder="abf.pos jo/2183" name="opt" value="{{ old('opt') }}">
              </div>
              <div class="form-group">
                <label for="inputPrice">*Pret</label>
                <input type="number" step="any" class="form-control" id="inputPrice" placeholder="0.00"  value="{{ old('price') }}" name="price" required>
              </div>
              <div class="form-group">
                <label for="currency">*Moneda</label>
                <select class="form-control" id="currency" name="currency" required>
                  <option value="EUR"@if(old('currency')=='EUR') selected @endif>EUR</option>
                  <option value="RON"@if(old('currency')=='RON') selected @endif>RON</option>
                </select>
              </div>
              <div class="form-group">
                <label for="inputDescription">Descriere</label>
                <textarea id="inputDescription" class="form-control" rows="3" placeholder="Descriere ..." name="description">{{ old('description') }}</textarea>
              </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Salveaza</button>
            </div>
          </form>
        </div>
      </div>
        <!-- /.box -->

    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

@endsection

@push('css')

  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  {{--<!-- Bootstrap time Picker -->--}}
  {{--<link rel="stylesheet" href="/plugins/timepicker/bootstrap-timepicker.min.css">--}}


@endpush

@push('scripts')
  <!-- bootstrap datepicker -->
  <script src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  {{--<!-- bootstrap time picker -->--}}
  {{--<script src="/plugins/timepicker/bootstrap-timepicker.min.js"></script>--}}

  <script>
      //Date picker
      $('#date').datepicker({
          format: 'dd/mm/yyyy',
          autoclose: true
      })
  </script>

  <script>
      $(document).ready(function(){
          $("#myInput").on("keyup", function() {
              // console.log('ok');
              var value = $(this).val().toLowerCase();
              $(".dropdown-menu li").filter(function() {
                  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
              });
          });
          $("#client-select li").click(function() {
              $("#client_id").val(this.id);
              $("#client_name").val($(this).find("a").text());
              $("#client-button").text($(this).find("a").text());
          });
      });
  </script>
@endpush