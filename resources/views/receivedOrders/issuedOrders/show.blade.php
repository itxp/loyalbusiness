@extends('layouts.main')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endpush

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Comenzi emise
      <small>Detalii comanda</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-6">
          <div class="box box-primary">
              <div class="box-header with-border">
                  <h3 class="box-title">Comanda numarul: <b>{{ $issuedOrder->id }}</b></h3>
                  <a href="{{ route('issuedOrders.pdfOpen', [$issuedOrder]) }}" class="btn btn-primary" target="_blank">Vizualizeaza</a>
                  <a href="{{ route('issuedOrders.pdfDownload', [$issuedOrder]) }}" class="btn btn-info">Descarca</a>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                  <i class="fa fa-calendar margin-r-5"></i>Data: <b>{{ $issuedOrder->date }}</b>

                  <hr>

                  <i class="fa fa-address-card margin-r-5"></i> Transportator: <b>{{ $issuedOrder->carrier->name }}</b>

                  <hr>

                  <i class="fa fa-money margin-r-5"></i> Pret: <b>{{ $issuedOrder->price }}</b> {{ $issuedOrder->currency }}

                  <hr>

                  <i class="fa fa-file-text-o margin-r-5"></i> Descriere:

                  <p>{{ $issuedOrder->description }}</p>

                  <hr>

                  <form action="{{ route('receivedOrders.issuedOrders.destroy', [ $receivedOrder, $issuedOrder ]) }}" method="post">
                      <input type="hidden" name="_method" value="delete">
                      {!! csrf_field() !!}
                      <a href="{{ route('receivedOrders.issuedOrders.edit', [ $receivedOrder, $issuedOrder ]) }}" class="btn btn-warning">Modifica</a>
                      <button class="btn btn-danger" type="submit">Sterge</button>
                  </form>
              </div>
              <!-- /.box-body -->
          </div>
          <div class="box box-primary">
              <div class="box-header with-border">
                  <h3 class="box-title">Marfuri</h3>

              </div>
              <!-- /.box-header -->
              <div class="box-body">
                  @foreach($issuedOrder->freights as $freight)
                      <p>
                          <b>#{{ $freight->id }}</b> Marfa: <b>{{ $freight->freight }}</b> Referinta: <b>{{ $freight->reference }}</b><br>
                          Ruta: <b>{{ $freight->loadCountry->name }}</b> to <b>{{ $freight->unloadCountry->name }}</b><br>
                          Expeditor: <b>{{ $freight->load_company }}</b> Destinatar: <b>{{ $freight->unload_company }}</b><br>
                          Incarcare(<b>{{ $freight->load_date }}</b>): <b>{{ $freight->load_address }}</b><br>
                          Descarcare(<b>{{ $freight->unload_date }}</b>): <b>{{ $freight->unload_address }}</b><br>
                          Descriere: {{ $freight->description }}<br>
                      </p>
                      <form action="{{ route('receivedOrders.issuedOrders.freights.destroy', [ $receivedOrder, $issuedOrder, $freight ]) }}" method="post">
                          <input type="hidden" name="_method" value="delete">
                          {!! csrf_field() !!}
                          <a href="{{ route('receivedOrders.issuedOrders.freights.edit', [ $receivedOrder, $issuedOrder, $freight ]) }}" class="btn btn-warning">Modifica</a>
                          <button class="btn btn-danger" type="submit">Sterge</button>
                      </form>

                      <hr>
                  @endforeach
{{--                  <a href="{{ route('receivedOrders.freights.create', [$issuedOrder->id]) }}" class="btn btn-primary">Adauga marfa</a>--}}
              </div>
              <!-- /.box-body -->
          </div>
      </div>
      <div class="col-md-6">
          <div class="box box-primary">
              <div class="box-header with-border">
                  <h3 class="box-title">Marfuri disponibile</h3>

              </div>
              <!-- /.box-header -->
              <div class="box-body">
                  @foreach($receivedOrder->freights as $freight)
                      <p>
                          <b>#{{ $freight->id }}</b> Marfa: <b>{{ $freight->freight }}</b> Referinta: <b>{{ $freight->reference }}</b><br>
                          Ruta: <b>{{ $freight->loadCountry->name }}</b> to <b>{{ $freight->unloadCountry->name }}</b><br>
                          Expeditor: <b>{{ $freight->load_company }}</b> Destinatar: <b>{{ $freight->unload_company }}</b><br>
                          Incarcare(<b>{{ $freight->load_date }} - {{ $freight->load_mentions }}</b>): <b>{{ $freight->load_address }}</b><br>
                          Descarcare(<b>{{ $freight->unload_date }} - {{ $freight->unload_mentions }}</b>): <b>{{ $freight->unload_address }}</b><br>
                          Descriere: {{ $freight->description }}
                      </p>
                      <a href="{{ route('receivedOrders.issuedOrders.freights.replicate', [ $receivedOrder, $issuedOrder, $freight ]) }}" class="btn btn-success">Adauga marfa</a>
                      <hr>
                  @endforeach
              </div>
              <!-- /.box-body -->
          </div>

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

@endsection
