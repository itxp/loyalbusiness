@extends('layouts.main')

@section('head-inject')
  <!-- DataTables -->
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Flota
      <small>Lista masini</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">General Elements</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Masina noua</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="POST" action="{{ route('receivedOrders.issuedOrders.store', [$receivedOrder->id]) }}">
            {{ csrf_field() }}
            <input type="hidden" id="carrier_id" name="carrier_id">
            <input type="hidden" id="user_id" name="user_id" value="{{ Auth::user()->id }}">
            <div class="box-body">
              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif
              <div class="form-group">
                <div class="dropdown">
                  <button id="carrier-button" class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">*Selecteaza transportator
                    <span class="caret"></span></button>
                  <ul class="dropdown-menu" id="carrier-select">
                    <input class="form-control" id="myInput" type="text" placeholder="Cauta...">

                    @foreach($carriers as $carrier)
                      <li id="{{ $carrier->id }}"><a href="#">{{ $carrier->name }}</a></li>
                    @endforeach

                  </ul>
                </div>
              </div>
              <div class="form-group">
                  <label for="inputNumber">*Numar</label>
                  <input type="number" step="any" class="form-control" id="inputNumber" name="number" value="{{ old('number')?old('number'):$lastNumber+1 }}" required>
              </div>
              <div class="form-group">
                <label for="date">*Data</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="date" name="date" value="{{ old('date')?old('date'):\Carbon\Carbon::now()->format('d/m/Y') }}" required>
                </div>
                <!-- /.input group -->
              </div>
              <div class="form-group">
                <label for="inputPrice">*Pret</label>
                <input type="number" step="any" class="form-control" id="inputPrice" placeholder="0.00"  value="{{ old('price') }}" name="price" required>
              </div>
              <div class="form-group">
                  <label for="currency">*Moneda</label>
                  <select class="form-control" id="currency" name="currency" required>
                      <option value="EUR"@if(old('currency')=='EUR') selected @endif>EUR</option>
                      <option value="RON"@if(old('currency')=='RON') selected @endif>RON</option>
                  </select>
              </div>
              <div class="form-group">
                  <label for="type">*Regim</label>
                  <select class="form-control" id="type" name="type" required>
                      <option value="Exclusiv"@if(old('currency')=='Exclusiv') selected @endif>Exclusiv</option>
                      <option value="Grupaj"@if(old('currency')=='Grupaj') selected @endif>Grupaj</option>
                  </select>
              </div>
              <div class="form-group">
                  <label for="inputPlate">Numar auto</label>
                  <input type="text" class="form-control" id="inputPlate" placeholder="B01ERU" name="plate" {{ old('plate') }}>
              </div>
              <div class="form-group">
                  <label for="inputDriver">Sofer</label>
                  <input type="text" class="form-control" id="inputDriver" placeholder="Gigi Alin" name="driver" value="{{ old('driver') }}">
              </div>

              <div class="form-group">
                <label for="inputDescription">Descriere</label>
                <textarea id="inputDescription" class="form-control" rows="3" placeholder="Descriere ..." name="description">{{ old('description') }}</textarea>
              </div>
              <div class="form-group">
                  <label for="inputTOS">Termeni si conditii</label>
                  <textarea id="inputTOS" class="form-control" rows="3" placeholder="Descriere ..." name="tos">@if(old('tos')){{ old('tos') }}@else MODALITATE DE PLATA: PLATA PRIN TRANSFER BANCAR(ORDIN DE PLATA)LA 45 DE ZILE DE LA DATA PRIMIRII FACTURII SI A CMR-ULUI CONFIRMAT FARA REZERVE, IN ORIGINAL.
Camionul poate pleca de la locul de incarcare numai dupa ce vom fi informati asupra volumului si greutatii marfii pe care a incarcat-o!
Transportatorul are obligatia sa ia in primire marfurile pe baza avizelor, facturilor de insotire, iar daca are rezerve privind starea marfurilor sa le mentioneze pe CMR in momentul incarcarii. In caz contrar se va considera ca marfurile nu prezentau vicii in momentul incarcarii.
Transportatorul este direct raspunzator de integritatea marfii pe parcursul derularii transportului.
Daca autoutilitara nu se prezinta la incarcat sau nu respecta termenele de incarcare si descarcare, in urma confirmarii prezentei comenzi, se percepe o penalizare de 400 Euro pe zi.
Aceasta comanda are valoare de contract. Confirmarea prezentei comenzi (prin stampila si semnatura) nu este necesara in cazul transmiterii electronice prin modem.
Prin contactarea directa a clientului se penalizeaza cu pana la 40 000 Euro.
VA ROG SA NE INSTIINTATI TELEFONIC LA NUMARUL DE TELEFON DE MAI JOS DUPA INCARCARE SI DESCARCARE! 0737445299 sau 0735445299
Camionul poate pleca de la locul de incarcare numai dupa ce vom fi informati asupra volumului si greutatii marfii pe care a incarcat-o acesta!
VA RUGAM SA NE TRIMITETI 2 CMR-uri IN ORIGINAL!
                      @endif
                  </textarea>
              </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Salveaza</button>
            </div>
          </form>
        </div>
      </div>
        <!-- /.box -->

    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

@endsection

@push('css')

  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  {{--<!-- Bootstrap time Picker -->--}}
  {{--<link rel="stylesheet" href="/plugins/timepicker/bootstrap-timepicker.min.css">--}}


@endpush

@push('scripts')
  <!-- bootstrap datepicker -->
  <script src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  {{--<!-- bootstrap time picker -->--}}
  {{--<script src="/plugins/timepicker/bootstrap-timepicker.min.js"></script>--}}

  <script>
      //Date picker
      $('#date').datepicker({
          format: 'dd/mm/yyyy',
          autoclose: true
      })
  </script>

  <script>
      $(document).ready(function(){
          $("#myInput").on("keyup", function() {
              // console.log('ok');
              var value = $(this).val().toLowerCase();
              $(".dropdown-menu li").filter(function() {
                  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
              });
          });
          $("#carrier-select li").click(function() {
              $("#carrier_id").val(this.id);
              $("#carrier-button").text($(this).find("a").text());
              // console.log($("#client-button").text("kkt"))
                  //= ($(this).find("a").text());
              // $("#client-button").html()
              // console.log($("#client-button").html());
          });
      });
  </script>
@endpush