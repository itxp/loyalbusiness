@extends('layouts.main')

@section('head-inject')
  <!-- DataTables -->
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Comenzi primite
      <small>Cursa noua</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Acasa</a></li>
        <li><a href="{{ route('receivedOrders.index') }}">Comenzi primite</a></li>
        <li><a href="{{ route('receivedOrders.show', [$receivedOrder->id]) }}">Comanda: #{{ $receivedOrder->number }}</a></li>
        <li class="active">Adauga cursa</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Cursa noua</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="POST" action="{{ route('receivedOrders.shipments.store', [$receivedOrder->id]) }}">
            {{ csrf_field() }}
            <div class="box-body">
              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif
              <div class="form-group">
                  <label for="inputCar">*Masina</label>
                  <select class="form-control" id="inputCar" name="car_id" required>
                      <option value="">Alege masina</option>
                      @foreach($cars as $car)
                          <option value="{{ $car->id }}"@if(old('car_id')==$car->id) selected @endif>{{ $car->plate }}</option>
                      @endforeach
                  </select>
              </div>
              <div class="form-group">
                  <label for="distance_unloaded">*Distanta Gol</label>
                  <input type="number" step="any" class="form-control" id="distance_unloaded" placeholder="0"  value="{{ old('distance_unloaded') }}" name="distance_unloaded" required>
              </div>
              <div class="form-group">
                  <label for="distance_loaded">*Distanta Plin</label>
                  <input type="number" step="any" class="form-control" id="distance_loaded" placeholder="0"  value="{{ old('distance_loaded') }}" name="distance_loaded" required>
              </div>
              <div class="form-group">
                <label for="inputPrice">*Pret</label>
                <input type="number" step="any" class="form-control" id="inputPrice" placeholder="0.00"  value="{{ old('price')?old('price'):$receivedOrder->price }}" name="price" required>
              </div>
              <div class="form-group">
                  <label for="currency">*Moneda</label>
                  <select class="form-control" id="currency" name="currency" required>
                      @if(old('currency'))
                          <option value="EUR"@if(old('currency')=='EUR') selected @endif>EUR</option>
                          <option value="RON"@if(old('currency')=='RON') selected @endif>RON</option>
                      @else
                          <option value="EUR"@if($receivedOrder->currency=='EUR') selected @endif>EUR</option>
                          <option value="RON"@if($receivedOrder->currency=='RON') selected @endif>RON</option>
                      @endif
                  </select>
              </div>

            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Salveaza</button>
            </div>
          </form>
        </div>
      </div>
        <!-- /.box -->

    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

@endsection

