@extends('layouts.main')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endpush

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Curse
      <small>Detalii cursa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Acasa</a></li>
        <li><a href="{{ route('receivedOrders.index') }}">Comenzi primite</a></li>
        <li><a href="{{ route('receivedOrders.show', [$receivedOrder->id]) }}">Comanda: #{{ $receivedOrder->number }}</a></li>
        <li class="active">Cursa #{{ $shipment->id }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-6">
          <div class="box box-primary">
              <div class="box-header with-border">
                  <h3 class="box-title">Cursa numarul: <b>{{ $shipment->id }}</b></h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">

                  <i class="fa fa-address-card margin-r-5"></i> Masina: <b>{{ $shipment->car->plate }}</b>

                  <hr>

                  <i class="fa fa-money margin-r-5"></i> Pret: <b>{{ $shipment->price }}</b> {{ $shipment->currency }}

                  <hr>

                  <i class="fa fa-road margin-r-5"></i> Distanta gol: <b>{{ $shipment->distance_unloaded }}</b> km

                  <hr>

                  <i class="fa fa-road margin-r-5"></i> Distanta plin: <b>{{ $shipment->distance_loaded }}</b> km

                  <hr>

                  <form action="{{ route('receivedOrders.shipments.destroy', [ $receivedOrder, $shipment ]) }}" method="post">
                      <input type="hidden" name="_method" value="delete">
                      {!! csrf_field() !!}
                      <a href="{{ route('receivedOrders.shipments.edit', [ $receivedOrder, $shipment ]) }}" class="btn btn-warning">Modifica</a>
                      <button class="btn btn-danger" type="submit">Sterge</button>
                  </form>

                  {{--<i class="fa fa-file-text-o margin-r-5"></i> Descriere:--}}

                  {{--<p>{{ $shipment->description }}</p>--}}
              </div>
              <!-- /.box-body -->
          </div>

          <div class="box box-primary">
              <div class="box-header with-border">
                  <h3 class="box-title">Marfuri</h3>

              </div>
              <!-- /.box-header -->
              <div class="box-body">
                  @foreach($shipment->freights as $freight)
                      <p>
                          <b>#{{ $freight->id }}</b> Marfa: <b>{{ $freight->freight }}</b> Referinta: <b>{{ $freight->reference }}</b><br>
                          Ruta: <b>{{ $freight->loadCountry->name }}</b> to <b>{{ $freight->unloadCountry->name }}</b><br>
                          Expeditor: <b>{{ $freight->load_company }}</b> Destinatar: <b>{{ $freight->unload_company }}</b><br>
                          Incarcare(<b>{{ $freight->load_date }}</b>): <b>{{ $freight->load_address }}</b><br>
                          Descarcare(<b>{{ $freight->unload_date }}</b>): <b>{{ $freight->unload_address }}</b><br>
                          Descriere: {{ $freight->description }}
                      </p>
                      <form action="{{ route('receivedOrders.shipments.freights.destroy', [ $receivedOrder, $shipment, $freight ]) }}" method="post">
                          <input type="hidden" name="_method" value="delete">
                          {!! csrf_field() !!}
                          <a href="{{ route('receivedOrders.shipments.freights.edit', [ $receivedOrder, $shipment, $freight ]) }}" class="btn btn-warning">Modifica</a>
                          <button class="btn btn-danger" type="submit">Sterge</button>
                      </form>

                      <hr>
                  @endforeach
{{--                  <a href="{{ route('receivedOrders.freights.create', [$issuedOrder->id]) }}" class="btn btn-primary">Adauga marfa</a>--}}
              </div>
              <!-- /.box-body -->
          </div>
      </div>
      <div class="col-md-6">
          <div class="box box-primary">
              <div class="box-header with-border">
                  <h3 class="box-title">Marfuri disponibile</h3>

              </div>
              <!-- /.box-header -->
              <div class="box-body">
                  @foreach($receivedOrder->freights as $freight)
                      <p>
                          <b>#{{ $freight->id }}</b> Marfa: <b>{{ $freight->freight }}</b> Referinta: <b>{{ $freight->reference }}</b><br>
                          Ruta: <b>{{ $freight->loadCountry->name }}</b> to <b>{{ $freight->unloadCountry->name }}</b><br>
                          Expeditor: <b>{{ $freight->load_company }}</b> Destinatar: <b>{{ $freight->unload_company }}</b><br>
                          Incarcare(<b>{{ $freight->load_date }} - {{ $freight->load_mentions }}</b>): <b>{{ $freight->load_address }}</b><br>
                          Descarcare(<b>{{ $freight->unload_date }} - {{ $freight->unload_mentions }}</b>): <b>{{ $freight->unload_address }}</b><br>
                          Descriere: {{ $freight->description }}
                      </p>
                      <a href="{{ route('receivedOrders.shipments.freights.replicate', [ $receivedOrder, $shipment, $freight ]) }}" class="btn btn-success">Adauga marfa</a>
                      <hr>
                  @endforeach
              </div>
              <!-- /.box-body -->
          </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

@endsection

