@extends('layouts.main')

@section('head-inject')
  <!-- DataTables -->
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Comenzi primite
      <small>Marfuri</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Acasa</a></li>
      <li><a href="{{ route('receivedOrders.index') }}">Comenzi primite</a></li>
      <li><a href="{{ route('receivedOrders.show', [$receivedOrder->id]) }}">Comanda: #{{ $receivedOrder->number }}</a></li>
      <li class="active">Adauga marfa</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Adauga marfa pentru comanda <b>{{ $receivedOrder->number }}</b></h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="POST" action="{{ route('receivedOrders.freights.store', [$receivedOrder->id]) }}">

            {{ csrf_field() }}

            {{--<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">--}}

            <div class="box-body">
              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif
              <div class="form-group">
                <label for="inputFreight">Marfa</label>
                <input type="text" class="form-control" id="inputFreight" placeholder="Ex. 2 paleti 110 x 60 x 50" value="{{ old('freight') }}" name="freight">
              </div>
              <div class="form-group">
                <label for="inputReference">Referinta</label>
                <input type="text" class="form-control" id="inputReference" placeholder="4178416" value="{{ old('reference') }}" name="reference">
              </div>
              <div class="form-group">
                <label for="inputDescription">Descriere</label>
                <textarea id="inputDescription" class="form-control" rows="3" placeholder="Descriere ..." name="description"></textarea>
              </div>
              <div class="form-group">
                <label for="load_date">*Data incarcare</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="load_date" name="load_date" value="{{ old('load_date')?old('load_date'):now()->format('d/m/Y') }}" required>
                </div>
                <!-- /.input group -->
              </div>
                <div class="form-group">
                  <label for="load_mentions">Mentiuni incarcare</label>
                  <input type="text" class="form-control" id="load_mentions" placeholder="de la 14:00 la 18:00" value="{{ old('load_mentions') }}" name="load_mentions">
                </div>
              <div class="form-group">
                <label for="load_country_id">*Tara incarcare</label>
                <select class="form-control" id="load_country_id" name="load_country_id" required>
                  <option value="">Selecteaza tara</option>
                  @foreach($countries as $country)
                    <option value="{{ $country->id }}"@if(old('country_id')==$country->id) selected @endif>{{ $country->name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="load_address">Adresa incarcare</label>
                <input type="text" class="form-control" id="load_address" placeholder="Koln 65432 etc..." value="{{ old('load_address') }}" name="load_address">
              </div>
              <div class="form-group">
                <label for="load_company">Companie incarcare</label>
                <input type="text" class="form-control" id="load_company" placeholder="Dacia Mioveni" value="{{ old('load_company') }}" name="load_company">
              </div>
              <div class="form-group">
                <label for="unload_date">*Data descarcare</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="unload_date" name="unload_date" value="{{ old('unload_date')?old('unload_date'):now()->format('d/m/Y') }}" required>
                </div>
                <!-- /.input group -->
              </div>
                <div class="form-group">
                  <label for="unload_mentions">Mentiuni descarcare</label>
                  <input type="text" class="form-control" id="unload_mentions" placeholder="de la 14:00 la 18:00" value="{{ old('unload_mentions') }}" name="unload_mentions">
                </div>
              <div class="form-group">
                <label for="unload_country_id">*Tara descarcare</label>
                <select class="form-control" id="unload_country_id" name="unload_country_id" required>
                  <option value="">Selecteaza tara</option>
                  @foreach($countries as $country)
                    <option value="{{ $country->id }}"@if(old('country_id')==$country->id) selected @endif>{{ $country->name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="unload_address">Adresa descarcare</label>
                <input type="text" class="form-control" id="unload_address" placeholder="Koln 65432 etc..." value="{{ old('unload_address') }}" name="unload_address">
              </div>
              <div class="form-group">
                <label for="unload_company">Companie descarcare</label>
                <input type="text" class="form-control" id="unload_company" placeholder="Renault Franta" value="{{ old('unload_company') }}" name="unload_company">
              </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Salveaza</button>
            </div>
          </form>
        </div>
      </div>
        <!-- /.box -->

    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

@endsection

@push('css')

  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

@endpush

@push('scripts')

  <!-- bootstrap datepicker -->
  <script src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script>
      //Date picker
      $('#load_date').datepicker({
          format: 'dd/mm/yyyy',
          autoclose: true
      })
      //Date picker
      $('#unload_date').datepicker({
          format: 'dd/mm/yyyy',
          autoclose: true
      })
  </script>

@endpush