@extends('layouts.main')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endpush

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Comenzi primite
      <small>Detalii comanda</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Acasa</a></li>
        <li><a href="{{ route('receivedOrders.index') }}">Comenzi primite</a></li>
        <li class="active">Comanda: #{{ $receivedOrder->number }}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-6">
          <div class="box box-primary">
              <div class="box-header with-border">
                  <h3 class="box-title">Comanda: <b>#{{ $receivedOrder->number }}</b></h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                  <i class="fa fa-calendar margin-r-5"></i>Data: <b>{{ $receivedOrder->date }}</b>

                  <hr>

                  <i class="fa fa-address-card margin-r-5"></i> Client: <b>{{ $receivedOrder->client->name }}</b>

                  <hr>

                  <i class="fa fa-info margin-r-5"></i> OPT/Extra: <b>{{ $receivedOrder->opt }}</b>

                  <hr>

                  <i class="fa fa-money margin-r-5"></i> Pret: <b>{{ $receivedOrder->price }}</b> {{ $receivedOrder->currency }}

                  <hr>

                  <i class="fa fa-file-text-o margin-r-5"></i> Factura:

                    @if(is_null($receivedOrder->invoice))
                      Nefacturata
                    @else
                      <a href="{{ route('invoices.show', [$receivedOrder->invoice]) }}">{{ $receivedOrder->invoice->series }}{{ $receivedOrder->invoice->number }} / {{ $receivedOrder->invoice->date }}</a>
                    @endif

                  <hr>

                  <i class="fa fa-file-text-o margin-r-5"></i> Descriere:

                  <p>{{ $receivedOrder->description }}</p>

                  <hr>

                  <form action="{{ route('receivedOrders.destroy', [ $receivedOrder ]) }}" method="post">
                      <input type="hidden" name="_method" value="delete">
                      {!! csrf_field() !!}
                      <a href="{{ route('receivedOrders.edit', [ $receivedOrder ]) }}" class="btn btn-warning confirm">Modifica</a>
                      <button class="btn btn-danger confirm" type="submit">Sterge</button>
                  </form>
              </div>
              <!-- /.box-body -->
          </div>
          <div class="box box-primary">
              <div class="box-header with-border">
                  <h3 class="box-title">Marfuri</h3>

              </div>
              <!-- /.box-header -->
              <div class="box-body">
                  @foreach($receivedOrder->freights as $freight)
                      <p>
                          <b>#{{ $freight->id }}</b> Marfa: <b>{{ $freight->freight }}</b> Referinta: <b>{{ $freight->reference }}</b><br>
                          Ruta: <b>{{ $freight->loadCountry->name }}</b> to <b>{{ $freight->unloadCountry->name }}</b><br>
                          Expeditor: <b>{{ $freight->load_company }}</b> Destinatar: <b>{{ $freight->unload_company }}</b><br>
                          Incarcare(<b>{{ $freight->load_date }} - {{ $freight->load_mentions }}</b>): <b>{{ $freight->load_address }}</b><br>
                          Descarcare(<b>{{ $freight->unload_date }} - {{ $freight->unload_mentions }}</b>): <b>{{ $freight->unload_address }}</b><br>
                          Descriere: {{ $freight->description }}
                      </p>
                      <form action="{{ route('receivedOrders.freights.destroy', [ $receivedOrder, $freight ]) }}" method="post">
                          <input type="hidden" name="_method" value="delete">
                          {!! csrf_field() !!}
                          <a href="{{ route('receivedOrders.freights.edit', [ $receivedOrder, $freight ]) }}" class="btn btn-warning confirm">Modifica</a>
                          <button class="btn btn-danger confirm" type="submit">Sterge</button>
                      </form>
                      <hr>
                  @endforeach
                  <a href="{{ route('receivedOrders.freights.create', [$receivedOrder->id]) }}" class="btn btn-success confirm">Adauga marfa</a>
              </div>
              <!-- /.box-body -->
          </div>
      </div>
      <div class="col-md-6">
          <div class="box box-primary">
              <div class="box-header with-border">
                  <h3 class="box-title">Curse</h3>

              </div>
              <!-- /.box-header -->
              <div class="box-body">
                  @foreach($receivedOrder->shipments as $shipment)
                      <p>
                          <b>#{{ $shipment->id }}</b> Masina: <b>{{ $shipment->car->plate }}</b> Pret: <b>{{ $shipment->price }}</b> {{ $shipment->currency }}
                      </p>
                      <form action="{{ route('receivedOrders.shipments.destroy', [ $receivedOrder, $shipment ]) }}" method="post">
                          <input type="hidden" name="_method" value="delete">
                          {!! csrf_field() !!}
                          <a href="{{ route('receivedOrders.shipments.show', [ $receivedOrder, $shipment ]) }}" class="btn btn-default">Deschide</a>
                          <a href="{{ route('receivedOrders.shipments.edit', [ $receivedOrder, $shipment ]) }}" class="btn btn-warning confirm">Modifica</a>
                          <button class="btn btn-danger confirm" type="submit">Sterge</button>
                      </form>
                      <hr>
                  @endforeach
                  <a href="{{ route('receivedOrders.shipments.create', [ $receivedOrder ]) }}" class="btn btn-success confirm">Adauga cursa</a>
              </div>
              <!-- /.box-body -->
          </div>
          <div class="box box-primary">
              <div class="box-header with-border">
                  <h3 class="box-title">Comenzi emise</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                  @foreach($receivedOrder->issuedOrders as $issuedOrder)
                      <p>
                          <b>#{{ $issuedOrder->id }}</b> Data: <b>#{{ $issuedOrder->date }}</b> Pret: <b>{{ $issuedOrder->price }}</b> {{ $issuedOrder->currency }}<br>
                          Transportator:<b>#{{ $issuedOrder->carrier->name }}</b>
                      </p>
                      <form action="{{ route('receivedOrders.issuedOrders.destroy', [ $receivedOrder, $issuedOrder ]) }}" method="post">
                          <input type="hidden" name="_method" value="delete">
                          {!! csrf_field() !!}
                          <a href="{{ route('receivedOrders.issuedOrders.show', [ $receivedOrder, $issuedOrder ]) }}" class="btn btn-default">Deschide</a>
                          <a href="{{ route('receivedOrders.issuedOrders.edit', [ $receivedOrder, $issuedOrder ]) }}" class="btn btn-warning confirm">Modifica</a>
                          <button class="btn btn-danger confirm" type="submit">Sterge</button>
                      </form>

                      <hr>
                  @endforeach
                  <a href="{{ route('receivedOrders.issuedOrders.create', [$receivedOrder]) }}" class="btn btn-success confirm">Emite comanda</a>
              </div>
              <!-- /.box-body -->
          </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

@endsection

@push('scripts')
    @if(!is_null($receivedOrder->invoice))
    <script type="text/javascript">
        $(document).ready(function () {
            $(".confirm").click(function (e) {
                // use whatever confirm box you want here
                if (!window.confirm("Comanda este facturata, doresti sa continui?")) {
                    e.preventDefault();
                }
            });
        });
    </script>
    @endif
@endpush

