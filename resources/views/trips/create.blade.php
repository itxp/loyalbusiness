@extends('layouts.main')

@section('head-inject')
  <!-- DataTables -->
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Deconturi
      <small>Adauga decont</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">General Elements</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Decont nou</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="POST" action="{{ route('trips.store') }}">
            {{ csrf_field() }}
            <input type="hidden" id="client_id" name="client_id">
            <input type="hidden" id="client_id" name="user_id" value="{{ Auth::user()->id }}">
            <div class="box-body">
              @if ($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif
              <div class="form-group">
                  <label for="date">*Data</label>
                  <div class="input-group date">
                      <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="date" name="date" value="{{ old('date')?old('date'):now()->format('d/m/Y') }}" required>
                  </div>
                  <!-- /.input group -->
              </div>
              <div class="form-group">
                  <label for="selectCar">*Masina</label>
                  <select class="form-control" id="selectCar" name="car_id" required>
                      <option value="">Selecteaza masina</option>
                      @foreach($cars as $car)
                        <option value="{{ $car->id }}"@if(old('car_id')==$car->id) selected @endif>{{ $car->plate }}</option>
                      @endforeach
                  </select>
              </div>
              <div class="form-group">
                  <label for="selectDriver">*Sofer</label>
                  <select class="form-control" id="selectDriver" name="driver_id" required>
                      <option value="">Selecteaza sofer</option>
                      @foreach($drivers as $driver)
                          <option value="{{ $driver->id }}"@if(old('driver_id')==$driver->id) selected @endif>{{ $driver->name }}</option>
                      @endforeach
                  </select>
              </div>
              <div class="form-group">
                <label for="inputRouteSheet">*Foaie parcurs</label>
                <input type="text" class="form-control" id="inputRouteSheet" placeholder="151" name="route_sheet" value="{{ old('route_sheet') }}" required>
              </div>
              <div class="form-group">
                  <label for="inputDisplacementOrder">*Ordin deplasare</label>
                  <input type="text" class="form-control" id="inputDisplacementOrder" placeholder="151" name="displacement_order" value="{{ old('displacement_order') }}" required>
              </div>
              <div class="form-group">
                <label for="date_start">*Data plecare</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="date_start" name="date_start" value="{{ old('date_start')?old('date_start'):now()->format('d/m/Y') }}" required>
                </div>
                <!-- /.input group -->
              </div>
              <div class="form-group">
                  <label for="date_end">*Data sosire</label>
                  <div class="input-group date">
                      <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="date_end" name="date_end" value="{{ old('date_end')?old('date_end'):now()->format('d/m/Y') }}" required>
                  </div>
                  <!-- /.input group -->
              </div>
              <div class="form-group">
                <label for="inputOdometerStart">*Km plecare</label>
                <input type="number" step="any" class="form-control" id="inputOdometerStart" placeholder="12312"  value="{{ old('odometer_start') }}" name="odometer_start" required>
              </div>
              <div class="form-group">
                  <label for="inputOdometerEnd">*Km sosire</label>
                  <input type="number" step="any" class="form-control" id="inputOdometerEnd" placeholder="17333"  value="{{ old('odometer_end') }}" name="odometer_end" required>
              </div>
                  <div class="form-group">
                      <label for="inputFuel">Carburant(L)</label>
                      <input type="number" step="any" class="form-control" id="inputFuel" placeholder="17333"  value="{{ old('fuel') }}" name="fuel" required>
                  </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Salveaza</button>
            </div>
          </form>
        </div>
      </div>
        <!-- /.box -->

    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

@endsection

@push('css')

  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  {{--<!-- Bootstrap time Picker -->--}}
  {{--<link rel="stylesheet" href="/plugins/timepicker/bootstrap-timepicker.min.css">--}}


@endpush

@push('scripts')
  <!-- bootstrap datepicker -->
  <script src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  {{--<!-- bootstrap time picker -->--}}
  {{--<script src="/plugins/timepicker/bootstrap-timepicker.min.js"></script>--}}

  <script>
      //Date picker
      $('#date').datepicker({
          format: 'dd/mm/yyyy',
          autoclose: true
      });
      //Date picker
      $('#date_start').datepicker({
          format: 'dd/mm/yyyy',
          autoclose: true
      });
      //Date picker
      $('#date_end').datepicker({
          format: 'dd/mm/yyyy',
          autoclose: true
      })
  </script>

@endpush