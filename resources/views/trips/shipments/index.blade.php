@extends('layouts.main')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
        Decont <a href="{{ route('trips.show', [ $trip->id]) }}">#{{ $trip->id }}</a>
      <small>Lista curse efectuate de {{ $trip->driver->name }} cu {{ $trip->car->plate }}</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">

          <!-- /.box-header -->
          <div class="box-body">
              <div class="box-header with-border">
                  <h3>Curse decont</h3>
              </div>
            <table class="table table-bordered" id="trip-shipments-table" width="100%">
              <thead>
              <tr>
                <th>Numar</th>
                <th>Pret</th>
                <th>Moneda</th>
                <th>Ruta</th>
                <th>Actiuni</th>
              </tr>
              </thead>
            </table>

          </div>

            <div class="box-body">
                <div class="box-header with-border">
                    <h3>Curse {{ $trip->car->plate }}</h3>
                </div>


                <table class="table table-bordered" id="car-shipments-table" width="100%">
                    <thead>
                    <tr>
                        <th>Numar</th>
                        <th>Pret</th>
                        <th>Moneda</th>
                        <th>Ruta</th>
                        <th>Actiuni</th>

                    </tr>
                    </thead>
                </table>

            </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

@endsection

@push('scripts')


  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
  <script src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>



  <script>
      $(function() {
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });

          var trip_table = $('#trip-shipments-table').DataTable({
              processing: true,
              serverSide: true,
              dom: 'Bfrtip',
              rowId: 'id',
              ajax: '{!! route('data.trips.shipments', [$trip->id]) !!}',
              columns: [
                  { data: 'id', name: 'id' },
                  { data: 'price', name: 'price' },
                  { data: 'currency', name: 'currency' },
                  { data: 'route', name: 'route' },
                  { data: 'action', name: 'action' },

              ],
              select:  {
                  style: 'multi'
              },

              "scrollX": true,
              "order": [[ 0, "desc" ]],

              buttons: [
                  {
                      text: 'Selecteaza tot',
                      action: function () {
                          this.rows().select();
                      }
                  },
                  {
                      text: 'Deselecteaza tot',
                      action: function () {
                          this.rows().deselect();
                      }
                  },
                  {
                      text: 'Scoate din decont',
                      action: function ( e, dt, items, indexes ) {
                          shipments = dt.rows( { selected: true } ).ids().toArray();
                          $.ajax({
                              type: 'POST',
                              url: '{!! route('data.trips.detachShipments', [$trip->id]) !!}',
                              data: { shipments: shipments },
                              success: function(response) {
                              }
                          });
                          dt.ajax.reload();
                          car_table.ajax.reload();
                      }
                  }
              ]



          });


          var car_table = $('#car-shipments-table').DataTable({
              processing: true,
              serverSide: true,
              rowId: 'id',
              dom: 'Bfrtip',
              ajax: '{!! route('data.trips.carShipments', [$trip->id]) !!}',
              columns: [
                  { data: 'id', name: 'id' },
                  { data: 'price', name: 'price' },
                  { data: 'currency', name: 'currency' },
                  { data: 'route', name: 'route' },
                  { data: 'action', name: 'action' },

              ],
              // 'autoWidth'   : true,

              select:  {
                  style: 'multi'
              },

              "scrollX": true,
              "order": [[ 0, "desc" ]],

              buttons: [
                  {
                      text: 'Selecteaza tot',
                      action: function () {
                          this.rows().select();
                      }
                  },
                  {
                      text: 'Deselecteaza tot',
                      action: function () {
                          this.rows().deselect();
                      }
                  },
                  {
                      text: 'Adauga la decont',
                      action: function ( e, dt, items, indexes ) {
                          shipments = dt.rows( { selected: true } ).ids().toArray();
                          $.ajax({
                              type: 'POST',
                              url: '{!! route('data.trips.attachShipments', [$trip->id]) !!}',
                              data: { shipments: shipments },
                              error: function() {
                                  alert('A aparut o eroare!')
                              }
                          });
                          dt.ajax.reload();
                          trip_table.ajax.reload();
                      }
                  }
              ]
          });
      });
  </script>

@endpush