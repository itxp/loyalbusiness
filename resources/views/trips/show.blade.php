@extends('layouts.main')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endpush

@section('content')

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
        Deconturi
      <small>Detalii decont (Curs valutar la data {{ $exchangeRate->date }}: <b>1</b> EUR = <b>{{ $exchangeRate->rate }}</b> RON)</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-6">
          <div class="box box-primary">
              <div class="box-header with-border">
                  <h3 class="box-title">Decont: <b>#{{ $trip->id }}</b></h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                  <i class="fa fa-calendar margin-r-5"></i>Data: <b>{{ $trip->date }}</b>
                  <hr>
                  <i class="fa fa-truck margin-r-5"></i>Masina: <b>{{ $trip->car->plate }}</b>
                  <hr>
                  <i class="fa fa-male margin-r-5"></i>Sofer: <b>{{ $trip->driver->name }}</b>
                  <hr>
                  <i class="fa fa-calendar margin-r-5"></i>Data plecare: <b>{{ $trip->date_start }}</b>
                  <hr>
                  <i class="fa fa-calendar margin-r-5"></i>Data sosire: <b>{{ $trip->date_end }}</b>
                  <hr>
                  <i class="fa fa-tachometer margin-r-5"></i>Km plecare: <b>{{ $trip->odometer_start }}</b>
                  <hr>
                  <i class="fa fa-tachometer margin-r-5"></i>Km sosire: <b>{{ $trip->odometer_end }}</b>
                  <hr>
                  <i class="fa fa-tachometer margin-r-5"></i>Km parcursi: <b>{{ $distance = $trip->odometer_end-$trip->odometer_start }}</b>
                  <hr>
                  <i class="fa fa-eyedropper margin-r-5"></i>Carburant: <b>{{ $trip->fuel }}</b> L
                  <hr>
                  <i class="fa fa-eyedropper margin-r-5"></i>Consum: <b>{{ round($trip->fuel/$distance*100, 2) }}</b> L/100Km
                  <hr>
                  <i class="fa fa-money margin-r-5"></i>Diurna: <b>{{ $payment = $distance*0.06 }}</b> EUR
                  <hr>
              </div>
              <!-- /.box-body -->

          </div>
          <div class="box box-primary">
              <div class="box-header with-border">
                  <h3 class="box-title">Curse</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                  <i class="fa fa-money margin-r-5"></i>EUR: <b>{{ $earnings_eur = $trip->shipments()->eur()->sum('price') }}</b>
                  <hr>
                  <i class="fa fa-money margin-r-5"></i>RON: <b>{{ $earnings_ron = $trip->shipments()->ron()->sum('price') }}</b> = <b>{{ $earnings_ron_eur = round($earnings_ron/$exchangeRate->rate,2) }}</b> EUR
                  <hr>
                  <i class="fa fa-money margin-r-5"></i>Total: <b>{{ $earnings = $earnings_ron_eur + $earnings_eur }}</b> EUR
                  <hr>
                  <i class="fa fa-tachometer margin-r-5"></i>Total Km: <b>{{ $trip->shipments->sum('distance_loaded')+$trip->shipments->sum('distance_unloaded') }}</b>
                  <hr>
                  <i class="fa fa-tachometer margin-r-5"></i>Total Km Plin: <b>{{ $trip->shipments->sum('distance_loaded') }}</b>
                  <hr>
                  <i class="fa fa-tachometer margin-r-5"></i>Total Km Gol: <b>{{ $trip->shipments->sum('distance_unloaded') }}</b>
                  <hr>
                  <a href="{{ route('trips.shipments.index', [ $trip->id ]) }}" class="btn btn-primary">Curse</a>
              </div>
              <!-- /.box-body -->
          </div>

      </div>
      <div class="col-md-6">
          <div class="box box-primary">
              <div class="box-header with-border">
                  <h3 class="box-title">Cheltuieli</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">

                  <i class="fa fa-money margin-r-5"></i>Total Cash: <b>{{ $expense_cash_eur = $trip->expenses()->cash()->eur()->sum('cost') }}</b> EUR + <b>{{ $expense_cash_ron = $trip->expenses()->cash()->ron()->sum('cost') }}</b> RON ({{ $expense_cash_ron_eur = round($expense_cash_ron / $exchangeRate->rate,2) }} EUR) = <b>{{ $expense_cash = $expense_cash_eur + $expense_cash_ron_eur }}</b> EUR
                  <hr>
                  <i class="fa fa-money margin-r-5"></i>Total DKV: <b>{{ $expense_dkv_eur = $trip->expenses()->dkv()->eur()->sum('cost') }}</b> EUR + <b>{{ $expense_dkv_ron = $trip->expenses()->dkv()->ron()->sum('cost') }}</b> RON ({{ $expense_dkv_ron_eur = round($expense_dkv_ron / $exchangeRate->rate,2) }} EUR) = <b>{{ $expense_dkv_eur + $expense_dkv_ron_eur }}</b> EUR
                  <hr>
                  <i class="fa fa-money margin-r-5"></i>Total Card Personal: <b>{{ $expense_personal_eur = $trip->expenses()->personal()->eur()->sum('cost') }}</b> EUR + <b>{{ $expense_personal_ron = $trip->expenses()->personal()->ron()->sum('cost') }}</b> RON ({{ $expense_personal_ron_eur = round($expense_personal_ron / $exchangeRate->rate,2) }} EUR) = <b>{{ $expense_personal = $expense_personal_eur + $expense_personal_ron_eur }}</b> EUR
                  <hr>
                  <i class="fa fa-money margin-r-5"></i>Total Card Firma: <b>{{ $expense_company_eur = $trip->expenses()->company()->eur()->sum('cost') }}</b> EUR <b>{{ $expense_company_ron = $trip->expenses()->company()->ron()->sum('cost') }}</b> RON  ({{ $expense_company_ron_eur = round($expense_company_ron / $exchangeRate->rate,2) }} EUR) = <b>{{ $expense_company_eur + $expense_company_ron_eur }}</b> EUR
                  <hr>
                  <i class="fa fa-money margin-r-5"></i>Total EUR: <b>{{ $expenses_eur = $trip->expenses()->eur()->sum('cost') }}</b>
                  <hr>
                  <i class="fa fa-money margin-r-5"></i>Total RON: <b>{{ $expenses_ron = $trip->expenses()->ron()->sum('cost') }}</b> = <b>{{ $expenses_ron_eur = round($expenses_ron/$exchangeRate->rate,2) }}</b> EUR
                  <hr>
                  <i class="fa fa-money margin-r-5"></i>Total: <b>{{ $expenses = $expenses_ron_eur + $expenses_eur }}</b> EUR
                  <hr>
                  <a href="{{ route('trips.expenses.index', [ $trip->id ]) }}" class="btn btn-primary">Cheltuieli</a>

              </div>
              <!-- /.box-body -->
          </div>
          <div class="box box-primary">
              <div class="box-header with-border">
                  <h3 class="box-title">Depuneri</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                  <i class="fa fa-money margin-r-5"></i>EUR: <b>{{ $amount_eur = $amount_eur = $trip->deposits()->eur()->sum('amount') }}</b>
                  <hr>
                  <i class="fa fa-money margin-r-5"></i>RON: <b>{{ $amount_ron = $trip->deposits()->ron()->sum('amount') }}</b> = <b>{{ $amount_ron_eur = round($amount_ron/$exchangeRate->rate,2) }}</b> EUR
                  <hr>
                  <i class="fa fa-money margin-r-5"></i>Total: <b>{{ $amount = $amount_eur + $amount_ron_eur }}</b> EUR
                  <hr>
                  <i class="fa fa-money margin-r-5"></i>Rest(Diurna-Depuneri): Diurna <b>{{ $payment }}</b> - Depuneri <b>{{ $amount }}</b> + Cheltuieli(Cash + Card Personal) <b>{{ $expense_driver = $expense_cash + $expense_personal }}</b> = <b>{{ $payment - $amount + $expense_driver }}</b> EUR
                  <hr>
                  <a href="{{ route('trips.deposits.index', [ $trip->id ]) }}" class="btn btn-primary">Depuneri</a>
              </div>
              <!-- /.box-body -->

          </div>

          <div class="box box-primary">
              <div class="box-header with-border">
                  <h3 class="box-title">Ramas</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                  <i class="fa fa-money margin-r-5"></i>Curse <b>{{ $earnings }}</b> - Cheltuieli <b>{{ $expenses }}</b> - Diurna <b>{{ $payment }}</b> = <b>{{ $earnings - $expenses - $payment }}</b> EUR
              </div>
              <!-- /.box-body -->
          </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->

@endsection

