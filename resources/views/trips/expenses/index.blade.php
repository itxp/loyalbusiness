@extends('layouts.main')

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endpush

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Deconturi
            <small>Cheltuieli</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Cheltuieli</h3>
                        <div>
                            <a href="{{ route('trips.expenses.create', [$trip->id]) }}" class="btn btn-success">Adauga cheltuiala</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="white-space:nowrap;">


                        <table class="table table-bordered" id="cars-table" width="100%">
                            <thead>
                            <tr>
                                <th>Nume</th>
                                <th>Data</th>
                                <th>Cost</th>
                                <th>Moneda</th>
                                <th>Sursa</th>
                            </tr>
                            </thead>
                        </table>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@endsection

@push('scripts')

    <!-- DataTables -->
    <script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

    <script>
        $(function() {
            $('#cars-table').DataTable({
                // responsive: true,
                processing: true,
                serverSide: true,
                ajax: '{!! route('data.trips.expenses', [$trip->id]) !!}',
                columns: [
                    { data: 'name', name: 'name' },
                    { data: 'date', name: 'date' },
                    { data: 'cost', name: 'cost' },
                    { data: 'currency', name: 'currency' },
                    { data: 'source', name: 'source' },
                ],
                "scrollX": true,
                "order": [[ 2, "desc" ]],

            });
        });
    </script>

@endpush